<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes with
| underscores in the controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;


$route['login'] =  'user/auth/getLoginInfo';
$route['logout']='user/auth/logout';
$route['signup']='user/auth/signup';
$route['get/details']='user/auth/update';
$route['update/details']='user/auth/save';
$route['search/universities']= 'search/university/find';
$route['university/(:num)'] = 'university/details/index';
$route['add/favourites'] = 'user/favourites/add';
$route['delete/favourites/(:num)'] = 'user/favourites/delete';
$route['get/favourites']= 'user/favourites/index';
$route['compare'] = 'university/compare/index';
$route['country/all'] = 'search/university/country';
$route['course/all'] = 'search/university/course';
$route['subcourse/(:num)'] = 'search/university/subcourse';
$route['get/degree'] = 'search/university/degree';
$route['countries'] = 'user/country_details/countries';
$route['states'] = 'user/country_details/states';
$route['cities']='user/country_details/cities';
$route['intake']='user/country_details/intake';

$route['admin/login'] = 'admin/auth/login';
$route['admin/userManagement'] = 'admin/auth/index';
$route['create/admin'] = 'admin/user_management/create';
$route['create/university'] = 'admin/user_management/create_university';
$route['create/representative']='admin/user_management/create_representative';
//$route['get/admins'] = 'admin/user_management/getadmins';
$route['get/admin/(:num)'] = 'admin/user_management/admin';
$route['edit/admin/(:num)'] = 'admin/user_management/adminedit';
$route['delete/admin/(:num)'] = 'admin/user_management/admindelete';
$route['delete/multiadmin']='admin/user_management/multiadmindelete';
$route['get/university/(:num)']='admin/university/university';
$route['edit/university/(:num)'] = 'admin/university/universityedit';
$route['delete/university/(:num)']='admin/university/universitydelete';
$route['delete/multiuniversity']='admin/university/multiuniversitydelete';
$route['get/representative/(:num)'] = 'admin/representative/getrepresentative';
$route['edit/representative/(:num)'] = 'admin/representative/editrepresentative';
$route['delete/representative/(:num)'] = 'admin/representative/deleterepresentative';
$route['delete/multirepresentative'] = 'admin/representative/multirepresentative';
$route['get/alumnus/(:num)'] = 'admin/alumnus/getalumnus';
$route['edit/alumnus/(:num)'] = 'admin/alumnus/editalumnus';
$route['delete/alumnus/(:num)'] = 'admin/alumnus/deletealumnus';
$route['delete/multiuniversity']  = 'admin/alumnus/multialumnus';

$route['get/intake'] = 'admin/intakemanagement/getintake';
$route['create/intake'] = 'admin/intakemanagementt/createintake';
$route['edit/intake/(:num)'] = 'admin/intakemanagement/edit';
$route['delete/intake/(:num)'] = 'admin/intakemanagement/delete';
$route['delete/multiintake'] = 'admin/intakemanagement/multidelete';

$route['get/country'] = 'admin/countrymanagement/country';
$route['create/country'] = 'admin/countrymanagement/create';
$route['edit/country/(:num)'] = 'admin/countrymanagement/edit';
$route['delete/country/(:num)'] = 'admin/countrymanagement/delete';
$route['delete/multicountry'] = 'admin/countrymanagement/multi';


$route['get/courses'] = 'admin/streammanagement/getstreams';
$route['delete/stream/(:num)'] = 'admin/streammanagement/delete';
$route['delete/multistream'] = 'admin/streammanagement/multi';

$route['get/university'] = 'user/auth/university';

$route['add/course'] = 'admin/coursemanagement/save';
$route['admin/user/student'] = 'admin/student/index';
