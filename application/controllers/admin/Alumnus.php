<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';



class Alumnus extends REST_Controller {

	function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model(array('login_model','user_model','auth_model','course_model','usermanagement_model','university_model'));
    }

    public function getalumnus_get(){
        $headerVariables = $this->head();
       if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
       {
           $error = array(
               "error_code" => "HEADER_MISSING",
               "error" => "Access Token Is Missing"
           );
           $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
       }
       $access_token = $headerVariables['Access-Token'];
       $access = $this->auth_model->getlogin($access_token);
       $userObj = json_decode($access,true);
       if (empty($userObj)) {
           $error = array(
               "error_code" => "SESSION_EXPIRED",
               "message" => "User Login Is Not Available"
           );

           $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
       }
       $conditions = array('user_master.id' => $this->uri->segment('3'));
       $user_details = $this->usermanagement_model->getUserByid($conditions);
       $this->response( $user_details, REST_Controller::HTTP_OK);
    }

    public function editalumnus_post(){
        $headerVariables = $this->head();
        $postVariables = $this->post();
        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
        {
            $error = array(
                "error_code" => "HEADER_MISSING",
                "error" => "Access Token Is Missing"
            );
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
        $access_token = $headerVariables['Access-Token'];
        $access = $this->auth_model->getlogin($access_token);
        $userObj = json_decode($access,true);
        if (empty($userObj)) {
            $error = array(
                "error_code" => "SESSION_EXPIRED",
                "message" => "User Login Is Not Available"
            );
 
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
        $data = array(
            'user_id' => $this->input->post('user_id'),
            'alumnu_id' => $this->uri->segment('3'),
            'university_id' => $universityId,
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'status' => $this->input->post('status'),
        );

        if($this->input->post('password'))
        {
            $data['password'] = md5($this->input->post('password'));
        }

        $this->university_model->editAlumnu($data);
        $this->response($data, REST_Controller::HTTP_OK);
    }

    public function deletealumnus_get(){
        $headerVariables = $this->head();
        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
        {
            $error = array(
                "error_code" => "HEADER_MISSING",
                "error" => "Access Token Is Missing"
            );
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
        $access_token = $headerVariables['Access-Token'];
        $access = $this->auth_model->getlogin($access_token);
        $userObj = json_decode($access,true);
        if (empty($userObj)) {
            $error = array(
                "error_code" => "SESSION_EXPIRED",
                "message" => "User Login Is Not Available"
            );
 
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
        $a= $this->university_model->deleteAlumnu($this->uri->segment('3'));
        $this->response($a, REST_Controller::HTTP_OK);

    }

    public function multialumnus_post(){
        $headerVariables = $this->head();
        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
       {
           $error = array(
               "error_code" => "HEADER_MISSING",
               "error" => "Access Token Is Missing"
           );
           $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
       }
       $access_token = $headerVariables['Access-Token'];
       $access = $this->auth_model->getlogin($access_token);
       $userObj = json_decode($access,true);
       if (empty($userObj)) {
           $error = array(
               "error_code" => "SESSION_EXPIRED",
               "message" => "User Login Is Not Available"
           );

           $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
       }
       $postVariables = $this->post();

       foreach ($postVariables as $value) {
        $status = $this->university_model->deleteAlumnu($value['id']);
       }
      
       $this->response($status,REST_CONTROLLER::HTTP_OK);

    }
}
?>