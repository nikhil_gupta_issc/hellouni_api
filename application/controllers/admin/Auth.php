<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Auth extends REST_Controller {

	public $outputData;

	function __construct()
    {
		
        // Construct the parent class
        parent::__construct();

        $this->load->model(array('login_model','user_model','auth_model','course_model','usermanagement_model'));
    }
    public function login_post()
    {
       
        $postVariables = $this->post();
        if(!isset($postVariables['username']) || !$postVariables['username'])
        {
            $error = array(
                "error_code" => "PARAMETER_MISSING",
                "message" => "Username Is Missing"
            );
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
        if(!isset($postVariables['password']) || !$postVariables['password'])
        {
            $error = array(
                "error_code" => "PARAMETER_MISSING",
                "message" => "Password Is Missing"
            );
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }

        $userName = $postVariables['username'];
        $password = md5($postVariables['password']);
               
        $userObj = $this->login_model->login($userName, $password);
        if($userObj[0]['type'] == 5 || !$userObj)
        {
            $error = array(
                "error_code" => "INVALID_INPUT",
                "message" => "EmailId Or Password Is Invalid."
            );
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        } 
        $key = md5($this->auth_model->generateUuid());
        $ttl = 3600;
        $loginObj = json_encode(["login_obj" => $userObj[0]]);
        $setLogin = $this->auth_model->setLogin($key, $loginObj, $ttl);

        $response['data']['login_object'] = $userObj[0];
        $response['data']['access_token'] = $key;
            $this->response($response, REST_Controller::HTTP_OK);


    }

    public function index_get(){
        $headerVariables = $this->head();
        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
        {
            $error = array(
                "error_code" => "HEADER_MISSING",
                "error" => "Access Token Is Missing"
            );
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
        $access_token = $headerVariables['Access-Token'];
        $access = $this->auth_model->getlogin($access_token);
        $userObj = json_decode($access,true);
        if (empty($userObj)) {
            $error = array(
                "error_code" => "SESSION_EXPIRED",
                "message" => "User Login Is Not Available"
            );

            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
      
   if( $userObj['login_obj']['type']==1){
                $cond_user1 = array('user_master.status !=' => '5'); 
                $cond_user2 = array('user_master.type' => '2'); 			
                $data['Admin'] = $this->usermanagement_model->getUser($cond_user1,$cond_user2);

                $cond_user3 = array('user_master.type' => '3');
                $data['University'] = $this->usermanagement_model->getUser($cond_user1,$cond_user3);

                $cond_user4 = array('user_master.type' => '4'); 
                $data['Representative'] = $this->usermanagement_model->getUser($cond_user1,$cond_user4);

                $cond_user5 = array('user_master.type' => '6');
                $data['Alumnus'] = $this->usermanagement_model->getUser($cond_user1,$cond_user5);
                $this->response($data, REST_Controller::HTTP_OK);

        }elseif($userObj['login_obj']['type']==2){
            $cond_user1 = array('user_master.status !=' => '5');
            $cond_user2 = array('user_master.type' => '3');
                $data['University'] = $this->usermanagement_model->getUser($cond_user1,$cond_user2);

                $cond_user4 = array('user_master.type' => '4'); 
                $data['Representative'] = $this->usermanagement_model->getUser($cond_user1,$cond_user4);

                $this->response($data, REST_Controller::HTTP_OK);

        }elseif ($userObj['login_obj']['type']==3) {
            $cond_user1 = array('user_master.status !=' => '5');
            //$cond_user2 = array('user_master.type' => '4');
            $cond_user2 = array('university_to_representative.university_id' => $userObj['login_obj']['id']); 
                
                $data['Representative'] = $this->usermanagement_model->getRepresentativeByUniversity($cond_user1,$cond_user2);

                $this->response($data, REST_Controller::HTTP_OK);

        }




}

    
}
?>