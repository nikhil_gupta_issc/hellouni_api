<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Countrymanagement extends REST_Controller {
    function __construct()
    {
		
        // Construct the parent class
        parent::__construct();

        $this->load->model(array('login_model','user_model','auth_model','course_model','usermanagement_model','country_model'));
    }

    public function country_get(){
        $headerVariables = $this->head();
        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
       {
           $error = array(
               "error_code" => "HEADER_MISSING",
               "error" => "Access Token Is Missing"
           );
           $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
       }
       $access_token = $headerVariables['Access-Token'];
       $access = $this->auth_model->getlogin($access_token);
       $userObj = json_decode($access,true);
       if (empty($userObj)) {
           $error = array(
               "error_code" => "SESSION_EXPIRED",
               "message" => "User Login Is Not Available"
           );

           $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
       }
       $data = $this->country_model->getcountry(array('status'=>1));
       $this->response($data,REST_Controller::HTTP_OK);

    }

    public function create_post(){
        $headerVariables = $this->head();
        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
       {
           $error = array(
               "error_code" => "HEADER_MISSING",
               "error" => "Access Token Is Missing"
           );
           $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
       }
       $access_token = $headerVariables['Access-Token'];
       $access = $this->auth_model->getlogin($access_token);
       $userObj = json_decode($access,true);
       if (empty($userObj)) {
           $error = array(
               "error_code" => "SESSION_EXPIRED",
               "message" => "User Login Is Not Available"
           );

           $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
       }
       $postVariables = $this->post();

       $data = array(
           "name" =>$postVariables['name'],
           "code" =>$postVariables['code'],
           "status" =>$postVariables['status']
       );
       $alreadyregistered = $this->country_model->checkCountry($data['name'],$data['code']);
         if($alreadyregistered){
             $error = array(
                 "message" => 'country already exists : '
             );
             $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
         }
             $Id = $this->country_model->insertCountry($data);
             $this->response($Id, REST_Controller::HTTP_OK);
            }


     public function edit_post(){
           $headerVariables = $this->head();
           if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
       {
           $error = array(
               "error_code" => "HEADER_MISSING",
               "error" => "Access Token Is Missing"
           );
           $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
       }
       $access_token = $headerVariables['Access-Token'];
       $access = $this->auth_model->getlogin($access_token);
       $userObj = json_decode($access,true);
       if (empty($userObj)) {
           $error = array(
               "error_code" => "SESSION_EXPIRED",
               "message" => "User Login Is Not Available"
           );

           $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
       }
       $postVariables = $this->post();

       $data = array(
           "id" =>$this->uri->segment('3'),
           "name" =>$postVariables['name'],
           "code" =>$postVariables['code'],
           "status" =>$postVariables['status']
       );
       $status = $this->country_model->editCountry($data);
       $this->response($status, REST_Controller::HTTP_OK);


       }     


    public function delete_get(){
        $headerVariables = $this->head();
        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
       {
           $error = array(
               "error_code" => "HEADER_MISSING",
               "error" => "Access Token Is Missing"
           );
           $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
       }
       $access_token = $headerVariables['Access-Token'];
       $access = $this->auth_model->getlogin($access_token);
       $userObj = json_decode($access,true);
       if (empty($userObj)) {
           $error = array(
               "error_code" => "SESSION_EXPIRED",
               "message" => "User Login Is Not Available"
           );

           $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
       }
       $status = $this->country_model->deleteCountry($this->uri->segment('3'));
       $this->response($status,REST_CONTROLLER::HTTO_OK);

    }   

    public function multi_post(){
        $headerVariables = $this->head();
        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
       {
           $error = array(
               "error_code" => "HEADER_MISSING",
               "error" => "Access Token Is Missing"
           );
           $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
       }
       $access_token = $headerVariables['Access-Token'];
       $access = $this->auth_model->getlogin($access_token);
       $userObj = json_decode($access,true);
       if (empty($userObj)) {
           $error = array(
               "error_code" => "SESSION_EXPIRED",
               "message" => "User Login Is Not Available"
           );

           $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
       }
       $postVariables = $this->post();

       foreach ($postVariables as $value) {
        $status = $this->country_model->deleteCountry($value['id']);
       }
      
       $this->response($status,REST_CONTROLLER::HTTP_OK);


    }
}



?>