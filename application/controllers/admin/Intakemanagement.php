<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Intakemanagement extends REST_Controller {
    function __construct()
    {
		
        // Construct the parent class
        parent::__construct();

        $this->load->model(array('auth_model','usermanagement_model','streammanagement_model'));
    }
    public function getintake_get(){
        $headerVariables = $this->head();
        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
        {
            $error = array(
                "error_code" => "HEADER_MISSING",
                "error" => "Access Token Is Missing"
            );
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
        $access_token = $headerVariables['Access-Token'];
        $access = $this->auth_model->getlogin($access_token);
        $userObj = json_decode($access,true);
        if (empty($userObj)) {
            $error = array(
                "error_code" => "SESSION_EXPIRED",
                "message" => "User Login Is Not Available"
            );
 
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
        $cond = array('intake_master.status !=' => '4');
        $data = $this->streammanagement_model->getIntakes($cond);
        //print_r($data);
        $this->response( $data, REST_Controller::HTTP_OK);

    }

    public function createintake_post(){
        $headerVariables = $this->head();
        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
        {
            $error = array(
                "error_code" => "HEADER_MISSING",
                "error" => "Access Token Is Missing"
            );
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
        $access_token = $headerVariables['Access-Token'];
        $access = $this->auth_model->getlogin($access_token);
        $userObj = json_decode($access,true);
        if (empty($userObj)) {
            $error = array(
                "error_code" => "SESSION_EXPIRED",
                "message" => "User Login Is Not Available"
            );
 
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
        $postVariables = $this->post();
        $data = array(
            "name" => $postVariables['name'],
            "status" => $postVariables['status']
        );

        $id = $this->streammanagement_model->insertIntake($data);
        $this->response($id, REST_Controller::HTTP_OK);


    }
    public function edit_post(){
        $headerVariables = $this->head();
        $headerVariables = $this->head();
        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
        {
            $error = array(
                "error_code" => "HEADER_MISSING",
                "error" => "Access Token Is Missing"
            );
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
        $access_token = $headerVariables['Access-Token'];
        $access = $this->auth_model->getlogin($access_token);
        $userObj = json_decode($access,true);
        if (empty($userObj)) {
            $error = array(
                "error_code" => "SESSION_EXPIRED",
                "message" => "User Login Is Not Available"
            );
 
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
        $postVariables = $this->post();
        $data = array(
            "name"=>$postVariables['name'],
            "status"=>$postVariables['status']
        );
        $Id = $this->streammanagement_model->updateIntake($data,array('intake_master.id'=>$this->uri->segment('3')));
        $this->response($Id, REST_Controller::HTTP_OK);

    }

    public function delete_get(){
        $headerVariables = $this->head();
        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
        {
            $error = array(
                "error_code" => "HEADER_MISSING",
                "error" => "Access Token Is Missing"
            );
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
        $access_token = $headerVariables['Access-Token'];
        $access = $this->auth_model->getlogin($access_token);
        $userObj = json_decode($access,true);
        if (empty($userObj)) {
            $error = array(
                "error_code" => "SESSION_EXPIRED",
                "message" => "User Login Is Not Available"
            );
    
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
        $data = array('status' => 4);
        $Id = $this->streammanagement_model->updateIntake($data,array('intake_master.id'=>$this->uri->segment('3')));
        $this->response($Id, REST_Controller::HTTP_OK);

    }

    public function multidelete_post(){
        $headerVariables = $this->head();
        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
     {
         $error = array(
             "error_code" => "HEADER_MISSING",
             "error" => "Access Token Is Missing"
         );
         $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
     }
     $access_token = $headerVariables['Access-Token'];
     $access = $this->auth_model->getlogin($access_token);
     $userObj = json_decode($access,true);
     if (empty($userObj)) {
         $error = array(
             "error_code" => "SESSION_EXPIRED",
             "message" => "User Login Is Not Available"
         );
 
         $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
     }
            $postVariables = $this->post();
                 foreach ($postVariables as  $value) {
              $data = $this->streammanagement_model->updateIntake(array('status' => 4),array('intake_master.id'=>$value['id']));
                }
         $this-> response($data, REST_Controller::HTTP_OK);
 
     
    }
}

?>