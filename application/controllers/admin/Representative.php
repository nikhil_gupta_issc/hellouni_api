<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Representative extends REST_Controller {
    function __construct()
    {
		
        // Construct the parent class
        parent::__construct();

        $this->load->model(array('login_model','user_model','auth_model','course_model','usermanagement_model'));
    }

    public function getrepresentative_get(){
        $headerVariables = $this->head();
       if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
       {
           $error = array(
               "error_code" => "HEADER_MISSING",
               "error" => "Access Token Is Missing"
           );
           $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
       }
       $access_token = $headerVariables['Access-Token'];
       $access = $this->auth_model->getlogin($access_token);
       $userObj = json_decode($access,true);
       if (empty($userObj)) {
           $error = array(
               "error_code" => "SESSION_EXPIRED",
               "message" => "User Login Is Not Available"
           );

           $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
       }
       $conditions = array('user_master.id' => $this->uri->segment('3'));
       $user_details = $this->usermanagement_model->getUserByid($conditions);
       $this->response( $user_details, REST_Controller::HTTP_OK);
    }

    public function editrepresentative_post(){
        $headerVariables = $this->head();
        $postVariables = $this->post();
        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
        {
            $error = array(
                "error_code" => "HEADER_MISSING",
                "error" => "Access Token Is Missing"
            );
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
        $access_token = $headerVariables['Access-Token'];
        $access = $this->auth_model->getlogin($access_token);
        $userObj = json_decode($access,true);
        if (empty($userObj)) {
            $error = array(
                "error_code" => "SESSION_EXPIRED",
                "message" => "User Login Is Not Available"
            );
    
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
        //print_r($postVariables);
        $postVariables['id'] = $this->uri->segment('3');
        $postVariables['type'] = 4;
        $postVariables['password'] = md5($postVariables['password']);
        $data = $this->usermanagement_model->editUser($postVariables);
        $this->response($data, REST_Controller::HTTP_OK);
    

    }

    public function deleterepresentative_get(){
        $headerVariables = $this->head();
        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
        {
            $error = array(
                "error_code" => "HEADER_MISSING",
                "error" => "Access Token Is Missing"
            );
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
        $access_token = $headerVariables['Access-Token'];
        $access = $this->auth_model->getlogin($access_token);
        $userObj = json_decode($access,true);
        if (empty($userObj)) {
            $error = array(
                "error_code" => "SESSION_EXPIRED",
                "message" => "User Login Is Not Available"
            );
    
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
        $data = $this->usermanagement_model->deleteUser($this->uri->segment(3));
        $this->response($data, REST_Controller::HTTP_OK);
    }

    public function multirepresentative_post(){
        $headerVariables = $this->head();
        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
     {
         $error = array(
             "error_code" => "HEADER_MISSING",
             "error" => "Access Token Is Missing"
         );
         $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
     }
     $access_token = $headerVariables['Access-Token'];
     $access = $this->auth_model->getlogin($access_token);
     $userObj = json_decode($access,true);
     if (empty($userObj)) {
         $error = array(
             "error_code" => "SESSION_EXPIRED",
             "message" => "User Login Is Not Available"
         );
 
         $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
     }
     $postVariables = $this->post();
     print_r($postVariables);
         foreach ($postVariables as  $value) {
              $data = $this->usermanagement_model->deleteUser($value['id']);
          }
         $this-> response($data, REST_Controller::HTTP_OK);
 
     }

    
}

?>