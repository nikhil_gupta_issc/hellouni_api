<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Streammanagement extends REST_Controller {
    function __construct()
    {
		
        // Construct the parent class
        parent::__construct();

        $this->load->model(array('auth_model','usermanagement_model','streammanagement_model'));
    }

    public function getstreams_get(){
        $headerVariables = $this->head();
        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
       {
           $error = array(
               "error_code" => "HEADER_MISSING",
               "error" => "Access Token Is Missing"
           );
           $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
       }
       $access_token = $headerVariables['Access-Token'];
       $access = $this->auth_model->getlogin($access_token);
       $userObj = json_decode($access,true);
       if (empty($userObj)) {
           $error = array(
               "error_code" => "SESSION_EXPIRED",
               "message" => "User Login Is Not Available"
           );

           $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
       }
       $data  = $this->streammanagement_model->getstreams(array('course_master.status !=' => '5'));
       $this->response($data,REST_Controller::HTTP_OK) ;
       }

    public function delete_get(){
        $headerVariables = $this->head();
        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
       {
           $error = array(
               "error_code" => "HEADER_MISSING",
               "error" => "Access Token Is Missing"
           );
           $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
       }
       $access_token = $headerVariables['Access-Token'];
       $access = $this->auth_model->getlogin($access_token);
       $userObj = json_decode($access,true);
       if (empty($userObj)) {
           $error = array(
               "error_code" => "SESSION_EXPIRED",
               "message" => "User Login Is Not Available"
           );

           $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
       }
       $data = $this->streammanagement_model->delete($this->uri->segment(3));
       echo $data;
    }
}
?>