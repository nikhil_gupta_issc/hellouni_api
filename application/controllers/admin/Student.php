<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';



class Student extends REST_Controller {

	function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model(array('login_model','user_model','auth_model','course_model'));
    }
}
?>