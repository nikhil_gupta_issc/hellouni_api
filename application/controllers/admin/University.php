<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class University extends REST_Controller {
    function __construct()
    {
		
        // Construct the parent class
        parent::__construct();

        $this->load->model(array('login_model','user_model','auth_model','course_model','usermanagement_model'));
    }

    public function university_get(){
        $headerVariables = $this->head();
       if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
       {
           $error = array(
               "error_code" => "HEADER_MISSING",
               "error" => "Access Token Is Missing"
           );
           $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
       }
       $access_token = $headerVariables['Access-Token'];
       $access = $this->auth_model->getlogin($access_token);
       $userObj = json_decode($access,true);
       if (empty($userObj)) {
           $error = array(
               "error_code" => "SESSION_EXPIRED",
               "message" => "User Login Is Not Available"
           );

           $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
       }
       $conditions = array('user_master.id' => $this->uri->segment('3'));
       $user_details = $this->usermanagement_model->getUserByid($conditions);
       $this->response( $user_details, REST_Controller::HTTP_OK);

    }

    public function universityedit_post(){
        $headerVariables = $this->head();
    $postVariables = $this->post();
    if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
    {
        $error = array(
            "error_code" => "HEADER_MISSING",
            "error" => "Access Token Is Missing"
        );
        $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
    }
    $access_token = $headerVariables['Access-Token'];
    $access = $this->auth_model->getlogin($access_token);
    $userObj = json_decode($access,true);
    if (empty($userObj)) {
        $error = array(
            "error_code" => "SESSION_EXPIRED",
            "message" => "User Login Is Not Available"
        );

        $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
    }
    //print_r($postVariables);
    $data = array(
        'id'            =>  $this->uri->segment('4'),
        'name' 			=>  $this->input->post('uniname'),
        'email' 		=>  $this->input->post('email'),
        'type' 			=>  '3',
        'status' 		=>	$this->input->post('status'),
        'view' 			=>	$this->input->post('view'),
        'create' 		=>	$this->input->post('creation'),
        'edit' 			=>	$this->input->post('edit'),
        'del' 			=>	$this->input->post('deletion'),
        'notification' 	=>	$this->input->post('notification'),
        'website' 		=>  $this->input->post('website'),
        'about' 		=>  $this->input->post('about'),
        'country_id' => $this->input->post('country'),
         'establishment_year' 		=>  $this->input->post('establishment_year') ? $this->input->post('establishment_year') : NULL,
         'university_type' 			=>  $this->input->post('type') ? $this->input->post('type') : NULL,
         'carnegie_accreditation' 	=>  $this->input->post('carnegie_accreditation'),
         'total_students' 		=>	$this->input->post('total_students') ? $this->input->post('total_students') : NULL,
         'total_students_UG' 			=>	$this->input->post('total_students_UG') ? $this->input->post('total_students_UG') : NULL,
         'total_students_PG' 		=>	$this->input->post('total_students_PG') ? $this->input->post('total_students_PG') : NULL,
         'total_students_international' 			=>	$this->input->post('total_students_international') ? $this->input->post('total_students_international') : NULL,
        'ranking_usa' 			=>	$this->input->post('ranking_usa') ? $this->input->post('ranking_usa') : NULL,
        'ranking_world' 			=>	$this->input->post('ranking_world') ? $this->input->post('ranking_world') : NULL,
         'admission_success_rate' 			=>	$this->input->post('admission_success_rate') ? $this->input->post('admission_success_rate') : NULL,
         'unique_selling_points' 	=>	$this->input->post('unique_selling_points'),
        'research_spending' 	=>	$this->input->post('research_spending') ? $this->input->post('research_spending') : NULL,
        'placement_percentage' 	=>	$this->input->post('placement_percentage') ? $this->input->post('placement_percentage') : NULL,
        'date_updated' 	=>	date('Y-m-d H:i:s'),
        'added_by' 	=>	1,
        'updated_by' 	=>	1

        );
        if($this->input->post('password')){
            $data['password'] = md5($this->input->post('password'));
        }
    $data = $this->usermanagement_model->editUser($data);
    $this->response($data, REST_Controller::HTTP_OK);

    }

    public function universitydelete_get(){
        $headerVariables = $this->head();
    if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
    {
        $error = array(
            "error_code" => "HEADER_MISSING",
            "error" => "Access Token Is Missing"
        );
        $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
    }
    $access_token = $headerVariables['Access-Token'];
    $access = $this->auth_model->getlogin($access_token);
    $userObj = json_decode($access,true);
    if (empty($userObj)) {
        $error = array(
            "error_code" => "SESSION_EXPIRED",
            "message" => "User Login Is Not Available"
        );

        $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
    }
    $data = $this->usermanagement_model->deleteUser($this->uri->segment(3));
    $this->response($data, REST_Controller::HTTP_OK);
    }


    public function multiuniversitydelete_post(){
        $headerVariables = $this->head();
       if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
    {
        $error = array(
            "error_code" => "HEADER_MISSING",
            "error" => "Access Token Is Missing"
        );
        $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
    }
    $access_token = $headerVariables['Access-Token'];
    $access = $this->auth_model->getlogin($access_token);
    $userObj = json_decode($access,true);
    if (empty($userObj)) {
        $error = array(
            "error_code" => "SESSION_EXPIRED",
            "message" => "User Login Is Not Available"
        );

        $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
    }
    $postVariables = $this->post();
    print_r($postVariables);
        foreach ($postVariables as  $value) {
             $data = $this->usermanagement_model->deleteUser($value['id']);
         }
        $this-> response($data, REST_Controller::HTTP_OK);

    }
    
}
?>