<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class User_management extends REST_Controller {



	function __construct()
    {
		
        // Construct the parent class
        parent::__construct();

        $this->load->model(array('login_model','user_model','auth_model','course_model','usermanagement_model'));
    }

    public function create_post(){
        $headerVariables = $this->head();
        $postVariables = $this->post();
        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
        {
            $error = array(
                "error_code" => "HEADER_MISSING",
                "error" => "Access Token Is Missing"
            );
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
        $access_token = $headerVariables['Access-Token'];
        $access = $this->auth_model->getlogin($access_token);
        $userObj = json_decode($access,true);
        if (empty($userObj)) {
            $error = array(
                "error_code" => "SESSION_EXPIRED",
                "message" => "User Login Is Not Available"
            );

            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
        $alreadyregistered = $this->login_model->logincheck($postVariables['email']);
            if($alreadyregistered){
                $error = array(
                    "message" => 'Admin already exists : '
                );
                $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
            }
        $data = array(
        	'name' 			=>  $postVariables['adminname'],
        	'email' 		=>  $postVariables['email'],
        	'password' 		=>  md5($postVariables['password']),
			'type' 			=>  '2',
			'createdate' 	=>  date('Y-m-d'),
			'status' 		=>	$postVariables['status'],
			'view' 			=>	$postVariables['view'],
			'create' 		=>	$postVariables['creation'],
			'edit' 			=>	$postVariables['edit'],
			'del' 			=>	$postVariables['deletion'],
			'notification' 	=>	$postVariables['notification'],
			
			
            );
            $Id=$this->usermanagement_model->insertUser($data);
            if($Id){
                $this->response($Id, REST_Controller::HTTP_OK);
            }
           

    }

   public function create_university_post(){
       $headerVariables = $this->head();
       $postVariables = $this->post();
        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
        {
            $error = array(
                "error_code" => "HEADER_MISSING",
                "error" => "Access Token Is Missing"
            );
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
        $access_token = $headerVariables['Access-Token'];
        $access = $this->auth_model->getlogin($access_token);
        $userObj = json_decode($access,true);
        if (empty($userObj)) {
            $error = array(
                "error_code" => "SESSION_EXPIRED",
                "message" => "User Login Is Not Available"
            );

            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
        $emailId= $postVariables['email'];
        $alreadyregistered = $this->login_model->logincheck($emailId);
            if($alreadyregistered){
                $error = array(
                    "message" => 'University already exists : '
                );
                $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
            }
            $data = array(
                'name' 			=>  $postVariables['name'],
                'email' 		=>  $postVariables['email'],
                'password' 		=>  md5($postVariables['password']),
                'type' 			=>  '3',
                'createdate' 	=>  date('Y-m-d'),
                'status' 		=>	$postVariables['status'],
                'view' 			=>	$postVariables['view'],
                'create' 		=>	$postVariables['creation'],
                'edit' 			=>	$postVariables['edit'],
                'del' 			=>	$postVariables['deletion'],
                'notification' 	=>	$postVariables['notification'],
                'country_id' => $this->input->post('country'),
            'website' 		=>  $this->input->post('website'),
            'about' 		=>  $this->input->post('about'),
            'establishment_year' 		=>  $this->input->post('establishment_year') ? $this->input->post('establishment_year') : NULL,
 			'university_type' 			=>  $this->input->post('type') ? $this->input->post('type') : NULL,
 			'carnegie_accreditation' 	=>  $this->input->post('carnegie_accreditation'),
 			'total_students' 		=>	$this->input->post('total_students') ? $this->input->post('total_students') : NULL,
 			'total_students_UG' 			=>	$this->input->post('total_students_UG') ? $this->input->post('total_students_UG') : NULL,
 			'total_students_PG' 		=>	$this->input->post('total_students_PG') ? $this->input->post('total_students_PG') : NULL,
 			'total_students_international' 			=>	$this->input->post('total_students_international') ? $this->input->post('total_students_international') : NULL,
            'ranking_usa' 			=>	$this->input->post('ranking_usa') ? $this->input->post('ranking_usa') : NULL,
            'ranking_world' 			=>	$this->input->post('ranking_world') ? $this->input->post('ranking_world') : NULL,
 			'admission_success_rate' 			=>	$this->input->post('admission_success_rate') ? $this->input->post('admission_success_rate') : NULL,
 			'unique_selling_points' 	=>	$this->input->post('unique_selling_points'),
            'research_spending' 	=>	$this->input->post('research_spending') ? $this->input->post('research_spending') : NULL,
            'placement_percentage' 	=>	$this->input->post('placement_percentage') ? $this->input->post('placement_percentage') : NULL,
            'date_created' 	=>	date('Y-m-d H:i:s'),
            'date_updated' 	=>	date('Y-m-d H:i:s'),
            'added_by' 	=>	1,
            'updated_by' 	=>	1
                
                
                );
                $Id=$this->usermanagement_model->insertUniDetails($data);
                if($Id){
                    $this->response($Id, REST_Controller::HTTP_OK);
                }


   } 
   public function create_representative_post(){
    $headerVariables = $this->head();
    $postVariables = $this->post();
     if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
     {
         $error = array(
             "error_code" => "HEADER_MISSING",
             "error" => "Access Token Is Missing"
         );
         $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
     }
     $access_token = $headerVariables['Access-Token'];
     $access = $this->auth_model->getlogin($access_token);
     $userObj = json_decode($access,true);
     if (empty($userObj)) {
         $error = array(
             "error_code" => "SESSION_EXPIRED",
             "message" => "User Login Is Not Available"
         );

         $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
     }
     $alreadyregistered = $this->login_model->logincheck($emailId,$mobileNumber);
         if($alreadyregistered){
             $error = array(
                 "message" => 'University already exists : '
             );
             $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
         }
         $data = array(
             'name' 			=>  $postVariables['name'],
             'email' 		=>  $postVariables['email'],
             'password' 		=>  md5($postVariables['password']),
             'type' 			=>  '4',
             'createdate' 	=>  date('Y-m-d'),
             'status' 		=>	$postVariables['status'],
             'view' 			=>	$postVariables['view'],
             'create' 		=>	$postVariables['creation'],
             'edit' 			=>	$postVariables['edit'],
             'del' 			=>	$postVariables['deletion'],
             'notification' 	=>	$postVariables['notification'],
             
             
             );
             $representative_id = $this->usermanagement_model->insert_Representative($data);
			
			    if($representative_id){
			
				foreach($this->input->post('courses') as $course){
			
				$courseData = array(
				'course_id' 			=>  $course,
				'representative_id' 	=>  $representative_id,
				'university_id' 		=>  $userdata['id']
				);
				
			
				$this->usermanagement_model->insertCourseToRepresentative($courseData);
			
			  }
			
			}

   }

   public function admin_get(){
       $headerVariables = $this->head();
       if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
       {
           $error = array(
               "error_code" => "HEADER_MISSING",
               "error" => "Access Token Is Missing"
           );
           $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
       }
       $access_token = $headerVariables['Access-Token'];
       $access = $this->auth_model->getlogin($access_token);
       $userObj = json_decode($access,true);
       if (empty($userObj)) {
           $error = array(
               "error_code" => "SESSION_EXPIRED",
               "message" => "User Login Is Not Available"
           );

           $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
       }
       $conditions = array('user_master.id' => $this->uri->segment('3'));
       $user_details = $this->usermanagement_model->getUserByid($conditions);
       $this->response( $user_details, REST_Controller::HTTP_OK);

   }

   public function adminedit_post(){
    $headerVariables = $this->head();
    $postVariables = $this->post();
    if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
    {
        $error = array(
            "error_code" => "HEADER_MISSING",
            "error" => "Access Token Is Missing"
        );
        $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
    }
    $access_token = $headerVariables['Access-Token'];
    $access = $this->auth_model->getlogin($access_token);
    $userObj = json_decode($access,true);
    if (empty($userObj)) {
        $error = array(
            "error_code" => "SESSION_EXPIRED",
            "message" => "User Login Is Not Available"
        );

        $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
    }
    $postVariables['id'] = $this->uri->segment('3');
    $postVariables['type'] = 2;
    $postVariables['password'] = md5($postVariables['password']);
    $data = $this->usermanagement_model->editUser($postVariables);
    $this->response($data, REST_Controller::HTTP_OK);
    
    
   }

   public function admindelete_get(){
    $headerVariables = $this->head();
    if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
    {
        $error = array(
            "error_code" => "HEADER_MISSING",
            "error" => "Access Token Is Missing"
        );
        $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
    }
    $access_token = $headerVariables['Access-Token'];
    $access = $this->auth_model->getlogin($access_token);
    $userObj = json_decode($access,true);
    if (empty($userObj)) {
        $error = array(
            "error_code" => "SESSION_EXPIRED",
            "message" => "User Login Is Not Available"
        );

        $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
    }
    $data = $this->usermanagement_model->deleteUser($this->uri->segment(3));
    $this->response($data, REST_Controller::HTTP_OK);

   }
   public function multiadmindelete_post(){
       $headerVariables = $this->head();
       if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
    {
        $error = array(
            "error_code" => "HEADER_MISSING",
            "error" => "Access Token Is Missing"
        );
        $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
    }
    $access_token = $headerVariables['Access-Token'];
    $access = $this->auth_model->getlogin($access_token);
    $userObj = json_decode($access,true);
    if (empty($userObj)) {
        $error = array(
            "error_code" => "SESSION_EXPIRED",
            "message" => "User Login Is Not Available"
        );

        $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
    }
    $postVariables = $this->post();
    print_r($postVariables);
        foreach ($postVariables as  $value) {
             $data = $this->usermanagement_model->deleteUser($value['id']);
         }
        $this-> response($data, REST_Controller::HTTP_OK);

   }

//    public function getadmins_get(){
//        $headerVariables = $this->head();
//        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
//     {
//         $error = array(
//             "error_code" => "HEADER_MISSING",
//             "error" => "Access Token Is Missing"
//         );
//         $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
//     }
//     $access_token = $headerVariables['Access-Token'];
//     $access = $this->auth_model->getlogin($access_token);
//     $userObj = json_decode($access,true);
//     if (empty($userObj)) {
//         $error = array(
//             "error_code" => "SESSION_EXPIRED",
//             "message" => "User Login Is Not Available"
//         );

//         $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
//     }

//     $cond_user1 = array('user_master.status !=' => '5'); 
//     $cond_user2 = array('user_master.type ' => '2');
//     $data = $this->usermanagement_model->getUser($cond_user1,$cond_user2);
//     print_r($data);
//     $this->response($data, REST_Controller::HTTP_OK);


//    }
}
?>