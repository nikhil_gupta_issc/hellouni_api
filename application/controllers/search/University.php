<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class University extends REST_Controller {

	public $outputData;

	function __construct()
    {
		
        // Construct the parent class
        parent::__construct();

        $this->load->model(array('login_model','user_model','auth_model','course_model'));
    }

    public function find_post(){
          $headerVariables = $this->head();
		  $postVariables = $this->post();
		  if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
		  {
			  $error = array(
				  "error_code" => "HEADER_MISSING",
				  "error" => "Access Token Is Missing"
			  );
			  $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
		  }
		  $access_token = $headerVariables['Access-Token'];
		 $access = $this->auth_model->getlogin($access_token);
         $userObj = json_decode($access,true);
            if (empty($userObj)) {
                $error = array(
                    "error_code" => "SESSION_EXPIRED",
                    "message" => "User Login Is Not Available"
                );
  
                $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
            }

            $data = array(
                "country "=>  $postVariables['country'],
                "mainstream"=> $postVariables['mainstream'],
                "subcourse"=> $postVariables['subcourse'],
                "degree"=> $postVariables['degree']
            );
			if($postVariables['search']){
				$courses = array('0'=>$postVariables['subcourse']); 
		        $degree = $postVariables['degree'];  
		        $country_array = array_map("strtoupper", array('0'=>$postVariables['country']));  //array('0'=>$this->input->post('country'));
				
			 }
			 
			 if($postVariables['search']){
		    foreach($courses as $substream){
				 $cond = array('university_to_degree_to_course.course_id' => $substream, 'university_to_degree_to_course.degree_id' => $degree);
                    
					 $details = $this->course_model->getuniversity_to_degree_to_course($cond);
			}
			if($details){
				$result = array();
				foreach ($details as $value=> $detail)  {
					$uniinfo = $this->course_model->getUniversityByid(array('user_master.id'=>$detail['university_id']));
					if (in_array($uniinfo->code, $country_array)){
										$result[$value]['university_id'] = $detail['university_id'];
										$result[$value]['university'] = $detail['name'];
										$result[$value]['degree']=$data['degree'];
										$result[$value]['course']=$data['mainstream'];
										$result[$value]['subcourse']=$data['subcourse'];
										$result[$value]['country_name']=$uniinfo->name;
										}
				}
			}
			
			 

    }
	$this->response($result, REST_Controller::HTTP_OK);

}


public function country_get(){
	$headerVariables = $this->head();
	$data = $this->course_model->getcountries(array('status'=>1));
	$this->response($data, REST_Controller::HTTP_OK);
}


public function course_get(){
	 $headerVariables = $this->head();
	  $data = $this->course_model->getcourses(array('status'=>1,'type'=>1));
	  $this->response($data, REST_Controller::HTTP_OK);

}


public function subcourse_get(){
	
	$headerVariables = $this->head();
	  $data = $this->course_model->getsubcourses(array('course_id'=>$this->uri->segment(2)));
	  $this->response($data, REST_Controller::HTTP_OK);

}


public function degree_get(){
	$headerVariables = $this->head();
	  $data = $this->course_model->getdegree(array('status'=>1));
	  $this->response($data, REST_Controller::HTTP_OK);

}

}

?>