<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Compare extends REST_Controller {

	public $outputData;

	function __construct()
    {
		
        // Construct the parent class
        parent::__construct();

        $this->load->model(array('login_model','user_model','auth_model','course_model'));
    }

   public function index_post(){
       $headerVariables = $this->head();
        $postVariables = $this->post();
        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
        {
			$error = array(
				"error_code" => "HEADER_MISSING",
				"error" => "Access Token Is Missing"
			);
			$this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
		}
        $access_token = $headerVariables['Access-Token'];
        $access = $this->auth_model->getlogin($access_token);
        $userObj = json_decode($access,true);
        if (empty($userObj)) {
            $error = array(
                "error_code" => "SESSION_EXPIRED",
                "message" => "User Login Is Not Available"
            );

            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        } 
        $postVariables = $this->post();
          $showdetails = $this->course_model->getuniversity(array('university_to_degree_to_course.university_id' => $postVariables['university_id']));
  
        
             
             $this->response($showdetails, REST_Controller::HTTP_OK);



   } 
}
?>