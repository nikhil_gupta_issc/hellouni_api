<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Details extends REST_Controller {

	public $outputData;

	function __construct()
    {
		
        // Construct the parent class
        parent::__construct();

        $this->load->model(array('login_model','user_model','auth_model','course_model'));
    }

    public function index_get(){
        $headerVariables = $this->head();
        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
        {
			$error = array(
				"error_code" => "HEADER_MISSING",
				"error" => "Access Token Is Missing"
			);
			$this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
		}
        $access_token = $headerVariables['Access-Token'];
        $access = $this->auth_model->getlogin($access_token);
        $userObj = json_decode($access,true);
        if (empty($userObj)) {
            $error = array(
                "error_code" => "SESSION_EXPIRED",
                "message" => "User Login Is Not Available"
            );

            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        } 
        $condition1 = array('user_master.id'=>$this->uri->segment(2),'user_master.status'=>1);
        $condition2 = array();
        $user_details = $this->course_model->checkUser($condition1);
       
        $uni_details = $this->course_model->getUniversityDetailsByid(array('university_id'=>$this->uri->segment(2)));
        $this->response($uni_details , REST_Controller::HTTP_OK);


    }
}
?>