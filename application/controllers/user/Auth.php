<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';



class Auth extends REST_Controller {

	function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model(array('login_model','user_model','auth_model','course_model'));
    }

    public function getLoginInfo_post(){
        $postVariables = $this->post();
        if(!isset($postVariables['username']) || !$postVariables['username'])
        {
            $error = array(
                "error_code" => "PARAMETER_MISSING",
                "message" => "Username Is Missing"
            );
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }
        if(!isset($postVariables['password']) || !$postVariables['password'])
        {
            $error = array(
                "error_code" => "PARAMETER_MISSING",
                "message" => "Password Is Missing"
            );
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }

        $userName = $postVariables['username'];
        $password = md5($postVariables['password']);
               
        $userObj = $this->login_model->login($userName, $password);
        if(!$userObj)
        {
            $error = array(
                "error_code" => "INVALID_INPUT",
                "message" => "EmailId Or Password Is Invalid."
            );
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        } 
        $key = md5($this->auth_model->generateUuid());
        $ttl = 3600;
        $loginObj = json_encode(["login_obj" => $userObj[0]]);
        $setLogin = $this->auth_model->setLogin($key, $loginObj, $ttl);

        $response['data']['login_object'] = $userObj[0];
        $response['data']['access_token'] = $key;
            $this->response($response, REST_Controller::HTTP_OK);


    }
    public function logout_post()
    {
        $headerVariables = $this->head();
        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
        {
			$error = array(
				"error_code" => "HEADER_MISSING",
				"error" => "Access Token Is Missing"
			);
			$this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
		}

		$token = $headerVariables['Access-Token'];

        $getLogin = $this->auth_model->getLogin($token);

        $userObj = json_decode($getLogin, true);

        if (empty($userObj)) {
            $error = array(
                "message" => "User Login Is Not Available"
            );

            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
        }

        $ttl = 0;
        $setLogin = $this->auth_model->setLogin($token, "", $ttl);
        $response['data']['access_token'] = "Success";

        $this->response($response, REST_Controller::HTTP_OK);
    }


    public function signup_post(){
        $postVariables = $this->post();
        $emailId = $postVariables['email'];
        $mobileNumber = $postVariables['mobile_number'];
        $num_length = strlen((string)$mobileNumber);
            if($num_length != 10) {
                $error = array(
                    "message" => "Invalid Mobile Number !"
                );

                $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
            }
            if(!filter_var($emailId, FILTER_VALIDATE_EMAIL))
            {
                $error = array(
                    "message" => "Invalid Email-Id !"
                );

                $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
            }
            $alreadyregistered = $this->login_model->logincheck($emailId,$mobileNumber);
            if($alreadyregistered){
                $error = array(
                    "message" => 'Lead already exists with ID: '
                );
                $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
            }
            $data = array(
                "name" => $postVariables["name"],
                "email"=>$postVariables["email"],
                "password"=> md5($postVariables["password"]),
                 "phone"=>$postVariables["mobile_number"],
                "image"=>"",
                "type"=>"5",
                " createdate"=>date('Y-m-d'),
                "activation"	=>	md5(time()),
                "fid"			=>  '',
                "status" 		=>	'3'
    
            );
            $user_id = $this->login_model->insertUserMaster($data);
            $student_data = array(
                "user_id" => $user_id,
                "address"=> $postVariables["address"],
                "city"=>$postVariables["city"],
                "zip"=>$postVariables["zip"],
                "dob"=>$postVariables["dob"],
                "phone"=>$postVariables["mobile_number"],
                "parent_name"=>$postVariables["parent_name"],
                "parent_mob"=>$postVariables["parent_mobile"],
                "parent_occupation"=>$postVariables["parent_occupation"],
                "hear_about_us"=>$postVariables["hear_info"],
                "passport"=>$postVariables["passport_availalibility"],
                "desired_destination"=>$postVariables["deisred_destinations"]
            );
            $student_id = $this->login_model->insertStudentDetails($student_data);

            if($postVariables["desired_course_name"])
            {
                
			    $desired_course = array(
                    "student_id"=>	$user_id,
                    "desired_course_name"=>$postVariables["desired_course_name"],
                    "desired_course_level"=>$postVariables["desired_coures_level"],
                    "desired_course_intake"=>$postVariables["desired_course_intake"],
                    "desired_course_year"=>$postVariables["desired_course_year"]

            );
            if(!$this->login_model->checkDesiredCourse(array('student_id'=>$user_id)))
                {			

                        $this->login_model->insertDesiredCourse($desired_course);		

                }   	
            }
            if($postVariables["secondary_institution_name"])
            {

                $secondary_qualification = array(
                    "student_id"=>$user_id,
                    "institution_name"=>$postVariables["secondary_institution_name"],
                    "board"=>$postVariables["board"],
                    "year_started"=>$postVariables["secondary_year_starrted"],
                    "year_finished"=>$postVariables["secondary_year_finished"],
                    "grade"=>$postVariables["secondary_grade"],
                    "total"=>$postVariables["secondary_total"]
				);	
                    if(!$this->login_model->checkSecondaryQualification(array('student_id'=>$user_id)))
                    {		
			            $this->login_model->insertSecondaryQualification($secondary_qualification);

			        }		
            }


            if($postVariables["diploma_id"]==1)
           {
                if($postVariables["diploma_course_name"])
                {
				$diploma_qualification = array(
                    "student_id"=>$user_id,
                    "course"=>$postVariables["diploma_course_name"],
                    "college"=>$postVariables["diploma_institution_name"],
                    "year_started"=>$postVariables["diploma_year_starrted"],
                    "year_finished"=>$postVariables["diploma_year_finished"],
                    "aggregate"=>$postVariables["diploma_grade"],
                    "total"=>$postVariables["diploma_total"]

				);
                    if(!$this->login_model->checkDiplomaQualification(array('student_id'=>$user_id)))
                    {	
                        
					   $this->login_model->insertDiplomaQualification($diploma_qualification);
				    }

                } 
        }else if($postVariables["diploma_id"]==2){
            if($postVariables["hs_institution_name"]){

                   $hs_qualification = array(
                    "student_id"=>$user_id,
                    "institution_name"=>$postVariables["hs_institution_name"],
                    "board"=>$postVariables["hs_board"],
                    "year_started"=>$postVariables["hs_year_starrted"],
                    "year_finished"=>$postVariables["hs_year_finished"],
                    "grade"=>$postVariables["hs_grade"],
                    "total"=>$postVariables["hs_total"]
                   );	

                   if(!$this->login_model->checkHigher_secondary_qualification(array('student_id'=>$user_id))){			

                       $this->login_model->insertHigher_secondary_qualification($hs_qualification);	

                   }

               }	 
        
            }
            if($postVariables["bachelor_course"]){
                $bachelor_qualification = array(
                    "student_id"=>$user_id,
                    "course"=>$postVariables["bachelor_course"],
                    "university"=>$postVariables["bachelor_university"],
                    "college"=>$postVariables["bachelor_college"],
                    "major"=>$postVariables["bachelor_major"],
                    "year_started"=>$postVariables["bachelor_year_starrted"],
                    "year_finished"=>$postVariables["bachelor_year_finished"],
                    "grade"=>$postVariables["bachelor_grade"],
                    "total"=>$postVariables["bachelor_total"]
                    );	  	
    
                
    
                    if(!$this->login_model->checkBachelorQualification(array('student_id'=>$user_id))){			   
    
                        $this->login_model->insertBachelorQualification($bachelor_qualification);				
    
                    }		 
    
                } 

                if($postVariables["master_course"]){

                    $master_qualification = array(
                        "student_id"=>$user_id,
                    "course"=>$postVariables["master_course"],
                    "university"=>$postVariables["master_university"],
                    "college"=>$postVariables["master_college"],
                    "major"=>$postVariables["master_major"],
                    "year_started"=>$postVariables["master_year_started"],
                    "year_finished"=>$postVariables["master_year_finished"],
                    "grade"=>$postVariables["master_grade"],
                    "total"=>$postVariables["master_total"]
                    );
                    if(!$this->login_model->checkMasterQualification(array('student_id'=>$user_id))){			   
    
                        $this->login_model->insertMasterQualification($master_qualification);					
    
                    }
    
                  }		 
    
               }


        public function update_get() {
            $headerVariables = $this->head();
            if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
            {
                $error = array(
                    "error_code" => "HEADER_MISSING",
                    "error" => "Access Token Is Missing"
                );
                $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
            }
            $access_token = $headerVariables['Access-Token'];
            $access = $this->auth_model->getlogin($access_token);
            $userObj = json_decode($access,true);
            if (empty($userObj)) {
                $error = array(
                    "error_code" => "SESSION_EXPIRED",
                    "message" => "User Login Is Not Available"
                );
    
                $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
            }
		    $condition = array('user_master.id'=>$userObj['login_obj']['id'],'user_master.status'=>1,'user_master.type'=>5);

            $userDetails = $this->login_model->getStudentDetails($condition);
            $this->response($userDetails,REST_Controller::HTTP_OK);
        }      



        public function save_post(){
            $headerVariables = $this->head();
            $postVariables = $this->post();
            if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
            {
                $error = array(
                    "error_code" => "HEADER_MISSING",
                    "error" => "Access Token Is Missing"
                );
                $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
            }
            $access_token = $headerVariables['Access-Token'];
            $access = $this->auth_model->getlogin($access_token);
            $userObj = json_decode($access,true);
            $b = $userObj['login_obj']['id'];
            if (empty($userObj)) {
                $error = array(
                    "error_code" => "SESSION_EXPIRED",
                    "message" => "User Login Is Not Available"
                );
  
                $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
            }
            // $details = array(
            //      'name' =>  $postVariables["name"]
                 
            // );	
    
            // $this->login_model->updateUserInfo($details,$userObj['login_obj']['id'])	;
    

            $data = array(

                "address"=> $postVariables["address"],
                "city"=>$postVariables["city"],
                "zip"=>$postVariables["zip"],
                "dob"=>$postVariables["dob"],
                "phone"=>$postVariables["mobile_number"],
                "parent_name"=>$postVariables["parent_name"],
                "parent_mob"=>$postVariables["parent_mobile"],
                "parent_occupation"=>$postVariables["parent_occupation"],
                "hear_about_us"=>$postVariables["hear_info"],
                "passport"=>$postVariables["passport_availalibility"],
                "desired_destination"=>$postVariables["deisred_destinations"]
    
            );
    
            $this->user_model->updateStudentDetails($data,$userObj['login_obj']['id']);

              //desired course
            if($postVariables["desired_course_name"]){
                $desired_course = array(
                    "student_id" => $userObj['login_obj']['id'],
                    "desired_course_name"=>$postVariables["desired_course_name"],
                    "desired_course_level"=>$postVariables["desired_coures_level"],
                    "desired_course_intake"=>$postVariables["desired_course_intake"],
                    "desired_course_year"=>$postVariables["desired_coures_year"]
    
                    );

                if($this->user_model->checkDesiredCourse(array('student_id'=>$userObj['login_obj']['id']))){
                    $this->user_model->updateDesiredCourse($desired_course,$userObj['login_obj']['id']);			
    
                }else{
                    $this->user_model->insertDesiredCourse($desired_course);
    
                }		
    
             }
            else{ 

                $this->login_model->deleteDesiredCourse($userObj['login_obj']['id']);
    
            }
            //secondary qualification details
    
            if($postVariables["secondary_institution_name"]){
    
            

                $secondary_qualification = array(
                    "student_id" => $userObj['login_obj']['id'],
                    "institution_name"=>$postVariables["secondary_institution_name"],
                    "board"=>$postVariables["board"],
                    "year_started"=>$postVariables["secondary_year_starrted"],
                    "year_finished"=>$postVariables["secondary_year_finished"],
                    "grade"=>$postVariables["secondary_grade"],
                    "total"=>$postVariables["secondary_total"]
    
                    );
    
            }
                if($this->user_model->checkSecondaryQualification(array('student_id'=>$userObj['login_obj']['id']))){	
                   
                    if($postVariables["secondary_institution_name"]){
                        $this->user_model->updateSecondaryQualification($secondary_qualification,$userObj['login_obj']['id']);	
    
                    }else{
                        $this->user_model->deleteSecondaryQualification($userObj['login_obj']['id']);	
    
                    }		
    
                }else{
                     if($postVariables["secondary_institution_name"]){
                        $this->user_model->insertSecondaryQualification($secondary_qualification);
    
                     }
                }		
    
            
    
            
    
            // Diploma Or 12th
    
            
    
             if($postVariables["diploma_id"]==1){
    
             
    
                 if($postVariables["diploma_course_name"]){
    
                    $diploma_qualification = array(
                        "student_id" => $userObj['login_obj']['id'],
                        "course"=>$postVariables["diploma_course_name"],
                        "college"=>$postVariables["diploma_institution_name"],
                        "year_started"=>$postVariables["diploma_year_starrted"],
                        "year_finished"=>$postVariables["diploma_year_finished"],
                        "aggregate"=>$postVariables["diploma_grade"],
                        "total"=>$postVariables["diploma_total"]
                    );
    
                }
    
                
    
                if($this->user_model->checkDiplomaQualification(array('student_id'=>$userObj['login_obj']['id']))){			
    
                    if($postVariables["diploma_course_name"]){
    
                        $this->user_model->updateDiplomaQualification($diploma_qualification,$userObj['login_obj']['id']);	
    
                    }else{				
    
                        $this->user_model->deleteDiplomaQualification($userObj['login_obj']['id']);	
    
                    }		
    
                }else{
    
                     if($postVariables["diploma_course_name"]){
    
                        $this->user_model->insertDiplomaQualification($diploma_qualification);
    
                     }
    
                }
    
                
    
             
    
                 //delete else here
    
                 if($this->user_model->checkHigher_secondary_qualification(array('student_id'=>$userObj['login_obj']['id']))){
    
                    $this->user_model->deleteHigher_secondary_qualification($userObj['login_obj']['id']);
    
                 }
    
             
    
             
             }else if($postVariables["diploma_id"]==2){
    
             
    
                 if($postVariables["hs_institution_name"]){
    
                        $hs_qualification = array(
                            "student_id" => $userObj['login_obj']['id'],
                    "institution_name"=>$postVariables["hs_institution_name"],
                    "board"=>$postVariables["hs_board"],
                    "year_started"=>$postVariables["hs_year_starrted"],
                    "year_finished"=>$postVariables["hs_year_finished"],
                    "grade"=>$postVariables["hs_grade"],
                    "total"=>$postVariables["hs_total"]
    
                        );
    
                    }
    
                    
    
                    if($this->user_model->checkHigher_secondary_qualification(array('student_id'=>$userObj['login_obj']['id']))){			
    
                        if($postVariables["hs_institution_name"]){
    
                            $this->user_model->updateHigher_secondary_qualification($hs_qualification,$userObj['login_obj']['id']);	
    
                        }else{				
    
                            $this->user_model->deleteHigher_secondary_qualification($userObj['login_obj']['id']);	
    
                        }		
    
                    }else{
    
                         if($postVariables["hs_institution_name"]){
    
                            $this->user_model->insertHigher_secondary_qualification($hs_qualification);
    
                         }
    
                    }
    
             
    
                    //delete else here
    
                     if($this->user_model->checkDiplomaQualification(array('student_id'=>$userObj['login_obj']['id']))){
    
                        $this->user_model->deleteDiplomaQualification($userObj['login_obj']['id']);
    
                     }
    
             }else{
    
             
    
                 $this->user_model->deleteDiplomaQualification($userObj['login_obj']['id']);	
    
                $this->user_model->deleteHigher_secondary_qualification($userObj['login_obj']['id']);	
    
             
    
             }
    
            
    
            //Bachelor
    
            
    
            if($postVariables["bachelor_course"]){
    
            
    
                $bachelor_qualification = array(
                    "student_id" => $userObj['login_obj']['id'],
                "course"=>$postVariables["bachelor_course"],
                "university"=>$postVariables["bachelor_university"],
                "college"=>$postVariables["bachelor_college"],
                "major"=>$postVariables["bachelor_major"],
                "year_started"=>$postVariables["bachelor_year_starrted"],
                "year_finished"=>$postVariables["bachelor_year_finished"],
                "grade"=>$postVariables["bachelor_grade"],
                "total"=>$postVariables["bachelor_total"]
                    );
    
               }	
    
                
    
                if($this->user_model->checkBachelorQualification(array('student_id'=>$userObj['login_obj']['id']))){
                    if($postVariables["bachelor_course"]){
                        $this->user_model->updateBachelorQualification($bachelor_qualification,$userObj['login_obj']['id']);	
    
                    }else{				
    
                        $this->user_model->deleteBachelorQualification($userObj['login_obj']['id']);	
    
                    }		
    
                }else{
    
                     if($postVariables["bachelor_course"]){
    
                        $this->user_model->insertBachelorQualification($bachelor_qualification);
    
                     }
    
                }
    
                
    
                
    
                
    
                if($postVariables["masters"]==1){
                 if($postVariables["master_course"]){
                    $master_qualification = array(
                        "student_id" => $userObj['login_obj']['id'],
                    "course"=>$postVariables["master_course"],
                    "university"=>$postVariables["master_university"],
                    "college"=>$postVariables["master_college"],
                    "major"=>$postVariables["master_major"],
                    "year_started"=>$postVariables["master_year_started"],
                    "year_finished"=>$postVariables["master_year_finished"],
                    "grade"=>$postVariables["master_grade"],
                    "total"=>$postVariables["master_total"]
    
                    );
    
                }
    
                
    
                if($this->user_model->checkMasterQualification(array('student_id'=>$userObj['login_obj']['id']))){			
    
                    if($postVariables["bmaster_course"]){
    
                        $this->user_model->updateMasterQualification($master_qualification,$userObj['login_obj']['id']);	
    
                    }else{				
    
                        $this->user_model->deleteMasterQualification($userObj['login_obj']['id']);	
    
                    }		
    
                }else{
    
                     if($postVariables["master_course"]){
    
                        $this->user_model->insertMasterQualification($master_qualification);
    
                     }
    
                }		 
    
             
    
             }else{
    
             
    
                     $this->user_model->deleteMasterQualification($userObj['login_obj']['id']);		 
    
             
    
             }
    
            

        }
        public function university_post(){
                $headerVariables = $this->head();
            $postVariables = $this->post();

            $emailId = $postVariables['email'];
            $mobileNumber = $postVariables['phone'];
            $num_length = strlen((string)$mobileNumber);
            if($num_length != 10) {
                $error = array(
                    "message" => "Invalid Mobile Number !"
                );

                $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
            }
            if(!filter_var($emailId, FILTER_VALIDATE_EMAIL))
            {
                $error = array(
                    "message" => "Invalid Email-Id !"
                );

                $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
            }
            $alreadyregistered = $this->login_model->leadcheck($emailId,$mobileNumber);
            if($alreadyregistered){
                $error = array(
                    "message" => 'mobile number or email already exists : '
                );
                $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
            }
           
            $data = array(
                "name"=>$postVariables['name'],
                "phone"=>$postVariables['phone'],
                "email"=>$postVariables['email'],
                "countries"=> implode(',',$postVariables['countries']),
                "courses"=>implode(',',$postVariables['courses']),
                "mainstream"=>$postVariables['mainstream'],
                "degree"=>$postVariables['degree'],
                
            );
            $user_id = $this->login_model->insertLeadMaster($data);
            $data1 = array(
                "name"=>$postVariables['name'],
                "phone"=>$postVariables['phone'],
                "email"=>$postVariables['email'],
                "password"=>md5($postVariables['password']),
                "image"			=>  '',
                "type" 			=>  '5',
                "createdate" 	=>  date('Y-m-d'),
                "activation" 	=>	md5(time()),
                "fid"			=>  '',
                "status" 		=>	'3'
            );
            // $user_id = $this->login_model->insertUserMaster($data1);
            // $student_data = array(
            //     "user_id"				=>	$user_id,
            //     "phone"      => $postVariables['phone']
            // );
            // $this->login_model->insertStudentDetails($student_data);
            
                    // $config=array(
                    //     'protocol'=>'smtp',
                    //     'smtp_host'=>'ssl://smtp.googlemail.com',
                    //     'smtp_port'=> 465,
                    //     'smtp_user'=>'',
                    //     'smtp_pass'=>''
                    // );
                    // $this->load->library('email',$config);
                    // $this->email->set_newline("\r\n");
                    // $this->email->from('lokeshbabu.gp95@gmail.com');
                    // $this->email->to($postVariables['email']);
                    // $this->email->subject('this is test mail');
                    // $this->email->message('sucessfully completed testing');
                    // $this->email->send();
                    
            $degree = $postVariables['degree'];
            $mainstream = $this->course_model->getmainstream(array('course_master.id'=>$postVariables['mainstream'],'course_master.type'=> 1));
            $countries = $postVariables['countries'];
            foreach ($postVariables['courses'] as  $value) {
                $cond = array('university_to_degree_to_course.course_id' => $value, 'university_to_degree_to_course.degree_id' => $degree);
                    $details[] = $this->course_model->getuniversity_to_degree_to_course($cond);
                     //$result = array();
            }
            foreach ($details as  $detail1) {
                    foreach ($detail1 as  $value) {
                        $uniinfo = $this->course_model->getUniversityByid(array('user_master.id'=>$value['university_id']));
                       
                if (in_array($uniinfo->code, $countries)){
                    $array = json_decode(json_encode($uniinfo), true);
                    $array['degree'] = $value['degree_name'];
                    $array['course'] =  $value['course_name'];
                    $array['mainstream']= $mainstream->name;
                    $result[] = $array;    
                    }
                }
               
            }
            $this->response($result, REST_Controller::HTTP_OK);
        }

      
        


   }

?>