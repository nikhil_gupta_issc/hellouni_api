<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Country_details extends REST_Controller {

	public $outputData;

	function __construct()
    {
		
        // Construct the parent class
        parent::__construct();

        $this->load->model(array('login_model','user_model','auth_model','course_model','country_model'));
    }


    public function countries_get(){
        $headerVariables = $this->head();
        $country = $this->country_model->getcountries(array('status'=>1));
        $this->response( $country, REST_Controller::HTTP_OK);
    }

    public function states_post(){
        $headerVariables = $this->head();
        $postVariables = $this->post();
        $states = $this->country_model->getstatesBycountry(array('country_id' => $postVariables['country_id'],'status'=>1));
        $this->response( $states, REST_Controller::HTTP_OK);

    }

    public function cities_post(){
        $headerVariables = $this->head();
        $postVariables = $this->post();
        $cities = $this->country_model->getcitiesbystate(array('state_id'=>$postVariables['state_id'],'status'=>1));
        $this->response($cities, REST_Controller::HTTP_OK);

    }


    public function intake_get(){
        $country = $this->country_model->getintake(array('status'=>1));
        $this->response( $country, REST_Controller::HTTP_OK);

    }
    
}
?>