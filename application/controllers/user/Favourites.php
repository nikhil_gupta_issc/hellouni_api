<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Favourites extends REST_Controller {

	public $outputData;

	function __construct()
    {
		
        // Construct the parent class
        parent::__construct();

        $this->load->model(array('login_model','user_model','auth_model','course_model'));
    }


    public function index_get(){
        $headerVariables = $this->head();
        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
        {
			$error = array(
				"error_code" => "HEADER_MISSING",
				"error" => "Access Token Is Missing"
			);
			$this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
		}
        $access_token = $headerVariables['Access-Token'];
        $access = $this->auth_model->getlogin($access_token);
        $userObj = json_decode($access,true);
        if(empty($userObj))
        {
            $error = array(
                "error_code" => "SESSION_EXPIRED",
                "message" => "User Login Is Not Available"
            );
  
            $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);

        }
        $favorites = $this->course_model->getFavourites(array('favourites.user_id'=>$userObj['login_obj']['id']));

        $this->response($favorites ,  REST_Controller::HTTP_OK);
    }
    public function add_post(){
        $headerVariables = $this->head();
        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
        {
			$error = array(
				"error_code" => "HEADER_MISSING",
				"error" => "Access Token Is Missing"
			);
			$this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
		}
        $postVariables = $this->post();
        $access_token = $headerVariables['Access-Token'];
       $access = $this->auth_model->getlogin($access_token);
       $userObj = json_decode($access,true);
          if (empty($userObj)) {
              $error = array(
                  "error_code" => "SESSION_EXPIRED",
                  "message" => "User Login Is Not Available"
              );
    
              $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
          }
          $query = $this->course_model->checkFavourites(array('favourites.user_id'=>$userObj['login_obj']['id'],'favourites.university_id'=>$postVariables['university_id'],'favourites.degree'=>$postVariables['degree'],'favourites.course'=>$postVariables['course']));
         
          if(!$query){
            $data = array(
                'user_id' 		=>  $userObj['login_obj']['id'],
                'university_id' =>  $postVariables['university_id'],
                'course' 		=>  $postVariables['course'],
                'degree'		=>  $postVariables['degree'],			
                'createdate' 	=>  date('Y-m-d')
            );
            
            $favorite_id = $this->course_model->insertFavourites($data);
            $this->response($favorite_id, REST_Controller::HTTP_OK);
          }
            else{
                $error =array(
                    "error_code" =>"Favorites_Exits",
                    "message"=>"Favorites Already exits"
                );
                $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
            }
    
    
    }


    public function delete_get()
    {
        $headerVariables = $this->head();
        if(!isset($headerVariables['Access-Token']) || !$headerVariables['Access-Token'])
        {
			$error = array(
				"error_code" => "HEADER_MISSING",
				"error" => "Access Token Is Missing"
			);
			$this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
		}
        $access_token = $headerVariables['Access-Token'];
        $postVariables = $this->post();
        $access = $this->auth_model->getlogin($access_token);
        $userObj = json_decode($access,true);
          if (empty($userObj)) {
              $error = array(
                  "error_code" => "SESSION_EXPIRED",
                  "message" => "User Login Is Not Available"
              );
    
              $this->response($error, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
          }
          $id = $this->course_model->deleteFavourites( array('favourites.id'=>$this->uri->segment('3')));
          $sucess =array(
            "code" =>"Favorites_Deleted",
            "message"=>"Favorites Deleted Sucessfully"
             );
          $this->response($sucess, REST_Controller::HTTP_OK);
    }
}
?>