<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'third_party/predis/predis/autoload.php';


class Auth_model extends CI_Model {

	public function getLogin($key)
	{
    $client = new Predis\Client([
        'scheme' => 'tcp',
        'host'   => '127.0.0.1',
        'port'   => '6379',
    ]);

		if(empty($key)){
			return;
		}

    $exists = $client->exists($key);
    $value = $client->get($key);
    $ttl = $client->ttl($key);
    if($exists==1){
      $this->setTtl($key, $value, $ttl);
      return $value;
    }
    else{
      return;
    }
  }

	public function getTemplate($key)
	{
    $client = new Predis\Client([
        'scheme' => 'tcp',
        'host'   => '127.0.0.1',
        'port'   => '6379',
    ]);

		if(empty($key)){
			return;
		}

    $exists = $client->hexists('TEMPLATE',$key);
    $value = $client->hget('TEMPLATE',$key);
    //$ttl = $client->ttl($key);

    //if($exists==1){
      //$this->setTtl($key, $value, $ttl);
      return $value;
    //}
    //else{
      //return;
    //}
  }

  public function setLogin($key, $user_obj, $ttl)
	{
    $client = new Predis\Client([
        'scheme' => 'tcp',
        'host'   => '127.0.0.1',
        'port'   => '6379',
    ]);

		if(empty($key) || empty($user_obj) || empty($ttl)){
			return;
		}

    $client->set($key, $user_obj);
    $client->expire($key, $ttl);
    $exists = $client->exists($key);

    if($exists == 1){
			return $key;
		}
		else{
			return;
		}
  }


	public function setTemplate($key, $user_obj, $ttl)
	{
		$client = new Predis\Client([
				'scheme' => 'tcp',
				'host'   => '127.0.0.1',
				'port'   => '6379',
		]);

		if(empty($key) || empty($user_obj) || empty($ttl)){
			return;
		}

		$client->hset('TEMPLATE',$key, $user_obj);
		//$client->expire($key, $ttl);
		$exists = $client->hexists('TEMPLATE',$key);

		if($exists == 1){
			return $key;
		}
		else{
			return;
		}
	}


  public function delLogin($key)
	{
    $client = new Predis\Client([
        'scheme' => 'tcp',
        'host'   => '127.0.0.1',
        'port'   => '6379',
    ]);

    $success = $client->del($key);

    return $success;
  }

  public function generateUuid()
  {
    return uniqid(rand(0,mt_getrandmax()), true);
  }


  public function setTtl($key, $value, $ttl)
	{
    if($ttl < 3600){
      $client = new Predis\Client([
          'scheme' => 'tcp',
          'host'   => '127.0.0.1',
          'port'   => '6379',
      ]);
      $ttl_new = 3600;
      $client->set($key, $value);
      $client->expire($key, $ttl_new);
    }
  }

	public function setKey($key, $data)
	{
    $client = new Predis\Client([
        'scheme' => 'tcp',
        'host'   => '127.0.0.1',
        'port'   => '6379',
    ]);

		if(empty($key) || empty($data)){
			return;
		}

    $client->set($key, $data);
    $exists = $client->exists($key);

    if($exists == 1){
			return $key;
		}else{
			return;
		}
  }

	public function getKey($key)
	{
    $client = new Predis\Client([
        'scheme' => 'tcp',
        'host'   => '127.0.0.1',
        'port'   => '6379',
    ]);

		if(empty($key)){
			return;
		}

    $exists = $client->exists($key);

    if($exists==1){
			$value = $client->get($key);
      return $value;
    }else{
      return;
    }
  }



}
?>
