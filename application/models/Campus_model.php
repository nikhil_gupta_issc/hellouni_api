<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usermanagement_model extends CI_Model {
    public function editCampus($campusId, $campusData)
    {
        $this->db->where('id', $campusId);
   	    $this->db->update('campuses', $campusData);
    }
}
?>