<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Country_model extends CI_Model {


    public function getcountries($conditions=array()){
        $this->db->select('*');
        $this->db->from('location_country_master');	
        $this->db->where($conditions);
        $result=$this->db->get()->result_array();
		return $result;
    }

    public function getstatesBycountry($conditions=array())
    {
       
        $this->db->select('*');
        $this->db->from('state_master');	
        $this->db->where($conditions);
        $result=$this->db->get()->result_array();
		return $result;

    }
    public function getcitiesbystate($conditions = array()){
        $this->db->select('*');
        $this->db->from('city_master');	
        $this->db->where($conditions);
        $result=$this->db->get()->result_array();
		return $result;

    }

    public function getintake($conditions = array()){
        $this->db->select('*');
        $this->db->from('intake_master');	
        $this->db->where($conditions);
        $result=$this->db->get()->result_array();
		return $result;
    }

    public function insertCountry($insertData=array())
    {
    $this->db->insert('country_master', $insertData);
    return 'success';
    }


    public function checkCountry($name,$code){
        $this->db->select('*');
        $this->db->from('country_master');
        $this->db->where('name',$name);
        $this->db->or_where('code',$code);
                $query = $this->db->get();
		$result = $query->result_array();
                return $result;

    }

    public function editCountry($insertData=array())
    {
    $this->db->where('id',$insertData['id']);
    $this->db->update('country_master',$insertData);
    return 'success';
    }
    
	 public function deleteCountry($id)
	 {
	 $this->db->delete('country_master', array('id' => $id)); 
	 
	 	  
	 return 'success';
     }
     public function getcountry($conditions=array()){
         $this->db->select('*');
         $this->db->from('country_master');
         $this->db->where($conditions);
         $result=$this->db->get()->result_array();
		return $result;
     }
}
?>