<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course_model extends CI_Model {

    public function getSubCourses($condition1)
    {
        
       $this->db->where($condition1);
       $this->db->from('course_to_subCourse');
       $this->db->join('course_master', 'course_master.id = course_to_subCourse.subcourse_id','left');
       $this->db->join('status_master', 'status_master.id = course_master.status','left');
       $this->db->select('course_master.id as idd,course_master.name'); 
       $result=$this->db->get()->result_array(); 
       return $result;
           
    }

    public function getuniversity_to_degree_to_course ($conditions){
        $this->db->where($conditions);
        $this->db->from('university_to_degree_to_course');
        $this->db->join('degree_master','degree_master.id = university_to_degree_to_course.degree_id');
        $this->db->join('course_master','course_master.id = university_to_degree_to_course.course_id');
        $this->db->join('user_master', 'user_master.id = university_to_degree_to_course.university_id','left');
        $this->db->select('university_to_degree_to_course.id,university_to_degree_to_course.degree_id,university_to_degree_to_course.course_id,university_to_degree_to_course.university_id,university_to_degree_to_course.language,university_to_degree_to_course.admissionsemester,university_to_degree_to_course.beginning,university_to_degree_to_course.duration,university_to_degree_to_course.application_deadline,university_to_degree_to_course.details,university_to_degree_to_course.tutionfee,university_to_degree_to_course.enrolmentfee,university_to_degree_to_course.livingcost,university_to_degree_to_course.jobopportunities,university_to_degree_to_course.required_language,university_to_degree_to_course.academicrequirement,university_to_degree_to_course.academicrequirement,user_master.name,degree_master.name as degree_name,course_master.name as course_name');
        $result=$this->db->get()->result_array();
        return $result;
       }



     public function getUniversityByid ($conditions){
        $this->db->where($conditions);
        $this->db->from('user_master');
        $this->db->join('university_details', 'university_details.university_id = user_master.id','left');
        $this->db->join('country_master', 'country_master.id = university_details.country','left');
        $this->db->join('status_master', 'status_master.id = user_master.status','left');
        $this->db->select('user_master.id,user_master.name,user_master.email,user_master.type,university_details.about,university_details.country,country_master.code,country_master.name');
         $result=$this->db->get()->row();
        return $result;
   
       }
       public function getmainstream($condition){
         $this->db->where($condition);
         $this->db->from('course_master');
         $this->db->select('course_master.name');
         $result = $this->db->get()->row();
         return $result;
       }
       public function checkFavourites($conditions=array())

       { 
          $this->db->where($conditions);
          $this->db->from('favourites');
          $this->db->select('favourites.id,favourites.user_id,favourites.university_id,favourites.course');
          $result=$this->db->get()->row(); 
            return $result;
  
              
  
       }
       public function getCoursesRelatedToUniversityDegree ($conditions){
	
        $this->db->where($conditions);
        
        $this->db->from('university_to_degree_to_course');
        $this->db->join('course_master', 'course_master.id = university_to_degree_to_course.course_id','left');
        $this->db->join('status_master', 'status_master.id = course_master.status','left');
        $this->db->select('course_master.id,course_master.name,course_master.status,status_master.status as status_name');
           
        $result=$this->db->get()->result();
   
        return $result;
       }
       
       public function checkUser($conditions=array())

       {
          $this->db->where($conditions);
          $this->db->from('user_master');
          $this->db->join('type_master', 'type_master.type_id = user_master.type','left');
          $this->db->join('status_master', 'status_master.id = user_master.status','left');
          $this->db->select('user_master.id,user_master.name,user_master.email,user_master.image,user_master.type,type_master.type as typename,user_master.createdate,user_master.modifydate,user_master.status,status_master.status as status_name');
         $result=$this->db->get()->row();
          return $result;
  
       }

       public  function getUniversityDetailsByid ($conditions){
        $this->db->where($conditions);
        $this->db->from('university_details'); 
        $this->db->select('university_details.university_id,university_details.about,university_details.website,university_details.banner,university_details.student,university_details.internationalstudent,university_details.facilities,university_details.address,university_details.country,university_details.state,university_details.city');
        $result=$this->db->get()->row();
        return $result;
   
       }
      
       public function insertFavourites($data=array())

       { 
    
         $this->db->insert('favourites', $data);
    
         return $favouritesid = $this->db->insert_id();
    
       }

       public function deleteFavourites($condition)

       {
       $this->db->where($condition);
    
       if($this->db->delete('favourites'))
    
         return true;
    
       }



     public  function getFavourites($conditions=array())

	 {
		$this->db->where($conditions);
		$this->db->from('favourites');
		$this->db->join('user_master', 'user_master.id = favourites.university_id','left');
		$this->db->join('university_details', 'university_details.university_id = user_master.id','left');
		$this->db->join('country_master', 'country_master.id = university_details.country','left');
		$this->db->join('course_master', 'course_master.id = favourites.course','left');
		$this->db->join('degree_master', 'degree_master.id = favourites.degree','left');
		$this->db->select('favourites.id,favourites.user_id,favourites.university_id as uni_id,favourites.course,user_master.name,user_master.image,country_master.name as countryname,course_master.name as coursename,degree_master.name as degreename');
    $result=$this->db->get()->result_array();
    return $result;
    //return $result->result_array();

			

	 }
     
public function getuniversity($conditions=array())
{
  $this->db->where($conditions);
  $this->db->from('university_to_degree_to_course');
  $this->db->select('*');
  $result = $this->db->get()->result_array();
  return $result;
}

public function getcountries($conditions=array())
{
  $this->db->where($conditions);
  $this->db->from('country_master');
  $this->db->select('*');
  $result = $this->db->get()->result_array();
  return $result;

}


public function getcourses($conditions=array())
{
  $this->db->where($conditions);
  $this->db->from('course_master');
  $this->db->select('course_master.id,course_master.name');
  $result = $this->db->get()->result_array();
  return $result;
}

public function getdegree($conditions=array()){
  $this->db->where($conditions);
  $this->db->from('degree_master');
  $this->db->select('degree_master.id,degree_master.name');
  $result = $this->db->get()->result_array();
  return $result;
}
       
}
?>
