<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

        public function login($user,$pass)
        {
                $this->db->select('user_master.*,status_master.status as status_name,type_master.type as typename');
                $this->db->from('user_master ');
                $this->db->join('type_master', 'type_master.type_id = user_master.type','left');
	        $this->db->join('status_master', 'status_master.id = user_master.status','left');
                $this->db->group_start()
                        ->where('user_master.phone', $user)
                        ->or_where('user_master.email', $user)
                        ->group_end();
                $this->db->where('user_master.password', $pass);
                 $result = $this->db->get();
                 print_r($result->result_array());
                 return $result->result_array();
        }


        public function insertUserMaster($data)

        {
                $this->db->insert('user_master', $data);

                $user_id = $this->db->insert_id();	 

                return $user_id;	
        }

        public function logincheck($email)
        {

                $this->db->select('*');
                $this->db->from('user_master');
                $this->db->where('email',$email);
               // $this->db->or_where('phone',$mobile);
                $query = $this->db->get();
		$result = $query->result_array();
                return $result;
        }

        public function insertStudentDetails($insertData=array())

	 {	

		 $this->db->insert('student_details', $insertData);
		 return $this->db->insert_id(); 

	 }

         public  function checkDesiredCourse($conditions=array())
         {
                
                 $this->db->select('*');
                 $this->db->from('desired_courses');
                 $this->db->join('user_master', 'user_master.id = desired_courses.student_id');	
		$this->db->where($conditions);
		
		
               
		$result=$this->db->get()->row();
		return $result;

			

         }
          
         public function  insertDesiredCourse($data=array())

	 { 

	   $this->db->insert('desired_courses', $data);

	   return $desired_courses_id = $this->db->insert_id();

         }
         
         public function checkSecondaryQualification($conditions=array())	 
         { 
                
                	
                $this->db->select('*');
                $this->db->from('secondary_qualification');
                $this->db->join('user_master', 'user_master.id = secondary_qualification.student_id','left');	
		$this->db->where($conditions);
                $result=$this->db->get()->row();
		return $result;			

         }
         
         public function insertSecondaryQualification($data=array())

	 { 

	   $this->db->insert('secondary_qualification', $data);

	   return $secondary_qualification_id = $this->db->insert_id();

         }
         
         public function checkDiplomaQualification($conditions=array())	 {
                $this->db->select('*');	
                $this->db->from('diploma_qualification');
                $this->db->join('user_master', 'user_master.id = diploma_qualification.student_id','left');
		$this->db->where($conditions);
		$result=$this->db->get()->row();
		return $result;			

         }
         
         public function insertDiplomaQualification($data=array())

	 { 

	   $this->db->insert('diploma_qualification', $data);

	   return $diploma_qualification_id = $this->db->insert_id();

         }
         
         public function checkHigher_secondary_qualification($conditions=array())	 {
                $this->db->select('*');
                $this->db->from('higher_secondary_qualification');
                $this->db->join('user_master', 'user_master.id = higher_secondary_qualification.student_id','left');
		$this->db->where($conditions);
		$result=$this->db->get()->row();

		return $result;			

         }
         
         public function insertHigher_secondary_qualification($data=array())

	 { 

	   $this->db->insert('higher_secondary_qualification', $data);

	   return $higher_secondary_qualification_id = $this->db->insert_id();

         }
         
         public function checkBachelorQualification($conditions=array())	 {
                $this->db->select('*');
                $this->db->from('bachelor_qualification');
                $this->db->join('user_master', 'user_master.id = bachelor_qualification.student_id','left');	
		$this->db->where($conditions);
		$result=$this->db->get()->row();

		return $result;			

         }
         public function insertBachelorQualification($data=array())

	 { 

	   $this->db->insert('bachelor_qualification', $data);

	   return $secondary_qualification_id = $this->db->insert_id();

         }
         
         public function checkMasterQualification($conditions=array())	 {
                $this->db->select('*');	
                $this->db->from('master_qualification');
                $this->db->join('user_master', 'user_master.id = master_qualification.student_id','left');
		$this->db->where($conditions);
		$result=$this->db->get()->row();

		return $result;			

         }
         public function insertMasterQualification($data=array())

	 { 

	   $this->db->insert('master_qualification', $data);

	   return $secondary_qualification_id = $this->db->insert_id();

         }
         

         public function getStudentDetails($conditions=array())

         {             
                        $this->db->select('user_master.id,user_master.name,user_master.email,user_master.type,
                        type_master.type as typename,user_master.createdate,user_master.modifydate,user_master.status,
                        user_master.fid,status_master.status as status_name,student_details.address,
                        student_details.country,student_details.city,student_details.zip,student_details.dob,
                        student_details.phone,student_details.parent_name,student_details.parent_mob,
                        student_details.parent_occupation,student_details.hear_about_us,student_details.passport,
                        student_details.desired_destination');
                        $this->db->from('user_master');
                
                        $this->db->join('type_master', 'type_master.type_id = user_master.type','left');
                        $this->db->join('status_master', 'status_master.id = user_master.status','left');
                        $this->db->join('student_details', 'student_details.user_id = user_master.id','left');
                
                        $this->db->where($conditions);
                        $result=$this->db->get()->row();
                        return $result;
     
         }
            
         public function updateUserInfo($insertData=array(),$id)

	 {
                         $this->db->update('user_master',$insertData);
                        $this->db->where('id',$id); 
                        return true;
        }

         public function updateStudentDetails($insertData=array(),$id)
	 {
                $this->db->update('student_details',$insertData);       
                $this->db->where('user_id',$id);
                        return true;

         }

         
         public function updateDesiredCourse($insertData=array(),$id)

	 {	 

	 $this->db->where('student_id',$id);

	 $this->db->update('desired_courses',$insertData);	 

	  return true;

         }
         
         public function deleteDesiredCourse($student_id)

	 {

		 $this->db->where('student_id',$student_id);

		 if($this->db->delete('desired_courses'))

		    return true;

         }
         
         public function updateSecondaryQualification($insertData=array(),$id)

	 {	 

	 $this->db->where('student_id',$id);

	 $this->db->update('secondary_qualification',$insertData);	 

	  return true;

         }
         
         public function deleteSecondaryQualification($student_id)

	 {

		 $this->db->where('student_id',$student_id);

		 if($this->db->delete('secondary_qualification'))

		    return true;

         }
         public function updateDiplomaQualification($insertData=array(),$id)

	 {	 

	 $this->db->where('student_id',$id);

	 $this->db->update('diploma_qualification',$insertData);	 

	  return true;

         }
         
         public function deleteDiplomaQualification($student_id)

	 {

		 $this->db->where('student_id',$student_id);

		 if($this->db->delete('diploma_qualification'))

		    return true;

         }
         public function deleteHigher_secondary_qualification($student_id)

	 {

		 $this->db->where('student_id',$student_id);

		 if($this->db->delete('higher_secondary_qualification'))

		    return true;

         }
         
         public function updateHigher_secondary_qualification($insertData=array(),$id)

	 {	 

	 $this->db->where('student_id',$id);

	 $this->db->update('higher_secondary_qualification',$insertData);	 

	  return true;

         }


         public function updateBachelorQualification($insertData=array(),$id)

	 {	 

	 $this->db->where('student_id',$id);

	 $this->db->update('bachelor_qualification',$insertData);	 

	  return true;

	 }
         

         public function deleteBachelorQualification($student_id)

	 {

		 $this->db->where('student_id',$student_id);

		 if($this->db->delete('bachelor_qualification'))

		    return true;

         }
         public function updateMasterQualification($insertData=array(),$id)

	 {	 

	 $this->db->where('student_id',$id);

	 $this->db->update('master_qualification',$insertData);	 

	  return true;

         }
         
         public function deleteMasterQualification($student_id)

	 {

		 $this->db->where('student_id',$student_id);

		 if($this->db->delete('master_qualification'))

		    return true;

         }
         
        public function getUser($condition1,$condition2)
        {
                if(count($condition1)>0 || count($condition2)>0  || isset($condition1) || isset($condition2))
               {
            $this->db->where($condition1);
            $this->db->where($condition2);
         }
               
               
               $this->db->from('user_master');
               $this->db->join('type_master', 'type_master.type_id = user_master.type','left');
               $this->db->join('status_master', 'status_master.id = user_master.status','left');
               
               
               
               $this->db->select('user_master.id,user_master.name,user_master.email,user_master.type,type_master.type as typename,user_master.createdate,user_master.modifydate,user_master.status,status_master.status as status_name');
               
               
               
               $result=$this->db->get()->result();
               print_r($this->db->last_query());
               return $result;
                       
        }

        public function getRepresentativeByUniversity ($conditions1,$conditions2){
	
                $this->db->where($conditions1);
                $this->db->where($conditions2);
                $this->db->from('university_to_representative'); 
                $this->db->join('user_master', 'user_master.id = university_to_representative.representative_id','left');
                $this->db->join('status_master', 'status_master.id = user_master.status','left');
               
                       
                $this->db->select('user_master.id,university_to_representative.university_id,university_to_representative.representative_id,user_master.name,user_master.email,user_master.createdate,user_master.status,status_master.status as status_name');
                       
                $result=$this->db->get()->result();
                print_r($this->db->last_query());
                return $result;
               }
               public function leadcheck($email)
               {
       
                       $this->db->select('*');
                       $this->db->from('lead_master');
                       $this->db->where('email',$email);
                      // $this->db->or_where('phone',$mobile);
                       $query = $this->db->get();
                       $result = $query->result_array();
                       return $result;
               }
               public function insertLeadMaster($data)

               {
                       $this->db->insert('lead_master', $data);
       
                       $user_id = $this->db->insert_id();	 
       
                       return $user_id;	
               }
         
}
?>