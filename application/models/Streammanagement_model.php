<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Streammanagement_model extends CI_Model {

    public function getIntakes($condition)

    {
        $this->db->where($condition);
       $this->db->from('intake_master');
       $this->db->join('status_master', 'status_master.id = intake_master.status','left');
       $this->db->select('intake_master.id,intake_master.name,intake_master.status,status_master.status as status_name');
       $result=$this->db->get()->result();
       return $result;
    }

    public function insertIntake ($data){
        $this->db->insert('intake_master', $data);
        return $this->db->insert_id();
    }
    public function updateIntake($insertData=array(),$condition){
        $this->db->where($condition);
        $this->db->update('intake_master',$insertData);
        return true;
  
        }	

    public function getstreams($condition){
        $this->db->where($condition);
		$this->db->from('course_master');
		$this->db->join('course_type', 'course_type.id = course_master.type','left');
		$this->db->join('status_master', 'status_master.id = course_master.status','left');
		$this->db->select('course_master.id,course_master.name,course_master.details,course_master.type,course_type.type as typename,course_master.createdate,course_master.modifydate,course_master.status,status_master.status as status_name');
	    $result=$this->db->get()->result(); 
		return $result;


    } 
    public function delete($id){
        $status = array('status' => '5');
            $this->db->where('id',$id);
            $this->db->update('course_master',$status);
	        return 'success';
    }   
        
}
?>
