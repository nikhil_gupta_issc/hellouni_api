<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class University_model extends CI_Model {

    public function editAlumnu($alumnuData)
    {
        $user_master_data = array(
            'name' 		=> $alumnuData['name'],
            'email' 	=> $alumnuData['email'],
   	        'status' 	=> $alumnuData['status'],
   	        'flag'		=> '1'
        );

        if(isset($alumnuData['password']))
        {
            $user_master_data['password'] = $alumnuData['password'];
        }

        $this->db->start_cache();
        $this->db->where('id', $alumnuData['user_id']);
        $this->db->update('user_master', $user_master_data);
        $this->db->stop_cache();
   	    $this->db->flush_cache();

        $data = array(
            'university_id' => $alumnuData['university_id']
        );

        $this->db->start_cache();
        $this->db->where('id', $alumnuData['alumnu_id']);
        $this->db->update('alumnus', $data);
        $this->db->stop_cache();
   	    $this->db->flush_cache();
}

    public function deleteAlumnu($id){
        $this->db->where('id',$id);
        $this->db->delete('alumnus');
        return 'success';
    }
}
?>
    