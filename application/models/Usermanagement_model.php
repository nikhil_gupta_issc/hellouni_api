<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usermanagement_model extends CI_Model {

    public function getUser($condition1,$condition2)
	 {
	 	if(count($condition1)>0 || count($condition2)>0  || isset($condition1) || isset($condition2))
		{
	     $this->db->where($condition1);
	     $this->db->where($condition2);
	  }
		$this->db->from('user_master');
		$this->db->join('type_master', 'type_master.type_id = user_master.type','left');
		$this->db->join('status_master', 'status_master.id = user_master.status','left');
		$this->db->select('user_master.id,user_master.name,user_master.email,user_master.type,type_master.type as typename,user_master.createdate,user_master.modifydate,user_master.status,status_master.status as status_name');
	    $result=$this->db->get()->result_array();
		return $result;
			
     }
     
     public function getRepresentativeByUniversity ($conditions1,$conditions2){
	
        $this->db->where($conditions1);
        $this->db->where($conditions2);
        $this->db->from('university_to_representative'); 
        $this->db->join('user_master', 'user_master.id = university_to_representative.representative_id','left');
        $this->db->join('status_master', 'status_master.id = user_master.status','left');
       
           
        $this->db->select('user_master.id,university_to_representative.university_id,university_to_representative.representative_id,user_master.name,user_master.email,user_master.createdate,user_master.status,status_master.status as status_name');
           
        $result=$this->db->get()->result();
   
        return $result;
       }


       public function insertUser($insertData=array())
       {
                
       $user_master_data = array(
       'name' 		=> $insertData['name'],
       'email' 		=> $insertData['email'],
       'password' 	=> $insertData['password'],
       'type' 		=> $insertData['type'] ,
       'createdate' 	=> $insertData['createdate'],
       'status' 		=> $insertData['status'],
       'notification' => $insertData['notification'],
       'flag'			=> '1'
       );
       
       $this->db->insert('user_master', $user_master_data);
       $user_id = $this->db->insert_id();
       
       $user_permission_course_data = array(
       'user_id' 	=> $user_id,
       'view' 	=> $insertData['view'],
       'create' 	=> $insertData['create'],
       'edit' 	=> $insertData['edit'],
       'del' 		=> $insertData['del']
        );
       
       $this->db->insert('user_permission_course', $user_permission_course_data);
       return  $user_id;
       
       }
       public function insertUniDetails($insertData=array())
       {
                
       $user_master_data = array(
       'name' 		=> $insertData['name'],
       'email' 		=> $insertData['email'],
       'password' 	=> $insertData['password'],
       'type' 		=> $insertData['type'] ,
       'createdate' 	=> $insertData['createdate'],
       'status' 		=> $insertData['status'],
       'notification' => $insertData['notification'],
       'flag'			=> '1'
       );
       
       $this->db->insert('user_master', $user_master_data);
       $user_id = $this->db->insert_id();
       
       $user_permission_course_data = array(
       'user_id' 	=> $user_id,
       'view' 	=> $insertData['view'],
       'create' 	=> $insertData['create'],
       'edit' 	=> $insertData['edit'],
       'del' 		=> $insertData['del']
        );
       
       $this->db->insert('user_permission_course', $user_permission_course_data);
       $universityData = array(
         'user_id'	=> $user_id,
         'country_id' => $insertData['country_id'],
         'name' => $insertData['name'],
         'website' => $insertData['website'],
         'about' 		=>  $insertData['about'],
         'establishment_year' 		=>  $insertData['establishment_year'],
         'type' 			=>  $insertData['university_type'],
         'carnegie_accreditation' 	=>  $insertData['carnegie_accreditation'],
         'total_students' 		=>	$insertData['total_students'],
         'total_students_UG' 			=>	$insertData['total_students_UG'],
         'total_students_PG' 		=>	$insertData['total_students_PG'],
         'total_students_international' 			=>	$insertData['total_students_international'],
         'ranking_usa' 			=>	$insertData['ranking_usa'],
         'ranking_world' 			=>	$insertData['ranking_world'],
         'admission_success_rate' 			=>	$insertData['admission_success_rate'],
         'unique_selling_points' 	=>	$insertData['unique_selling_points'],
         'research_spending' 	=>	$insertData['research_spending'],
         'placement_percentage' 	=>	$insertData['placement_percentage'],
         'date_created' 	=>	$insertData['date_created'],
         'date_updated' 	=>	$insertData['date_updated'],
         'added_by' 	=>	$insertData['added_by'],
         'updated_by' 	=>	$insertData['updated_by']
      );
  
      $this->db->insert('universities', $universityData);
       
       
       return 'success';
       
       }
  
       public function insert_Representative($insertData=array())
       {
       
                
       $user_master_data = array(
       'name' 		=> $insertData['name'],
       'email' 		=> $insertData['email'],
       'password' 	=> $insertData['password'],
       'type' 		=> $insertData['type'] ,
       'createdate' 	=> $insertData['createdate'],
       'status' 		=> $insertData['status'],
       'notification' => $insertData['notification'],
       'flag'			=> '1'
       );
       
       $this->db->insert('user_master', $user_master_data);
       $user_id = $this->db->insert_id();
       
       $user_permission_course_data = array(
       'user_id' 	=> $user_id,
       'view' 	=> $insertData['view'],
       'create' 	=> $insertData['create'],
       'edit' 	=> $insertData['edit'],
       'del' 		=> $insertData['del']
        );
       
       $this->db->insert('user_permission_course', $user_permission_course_data);
       
       $uni_to_rep_data = array(
       'university_id' 		=> $insertData['university'],
       'representative_id'	=> $user_id
       );
       
       $this->db->insert('university_to_representative', $uni_to_rep_data);
       
       return $user_id;
       
       
       }

       public function insertCourseToRepresentative($insertData=array())
       {
            $this->db->insert('course_to_representative', $insertData);
          return $this->db->insert_id();
          
       }
       
       public function getUserByid ($conditions){
        $this->db->where($conditions);
        $this->db->from('user_master');
        $this->db->join('type_master', 'type_master.type_id = user_master.type','left');
        $this->db->join('status_master', 'status_master.id = user_master.status','left');
        $this->db->join('user_permission_course', 'user_permission_course.user_id = user_master.id','left');
        $this->db->select('user_master.id,user_master.name,user_master.email,user_master.image,user_master.about,user_master.phone,user_master.type,type_master.type as typename,user_master.createdate,user_master.modifydate,user_master.status,status_master.status as status_name,user_master.notification,user_permission_course.view,user_permission_course.create,user_permission_course.edit,user_permission_course.del');   
        $result=$this->db->get()->result_array();
        return $result;
       }

       public function editUser($insertData=array())
       {
         $user_master_data = array(
            'name' 		=> $insertData['name'],
            'email' 		=> $insertData['email'],
            'type' 		=> $insertData['type'] ,
            'status' 		=> $insertData['status'],
            'notification' => $insertData['notification']
            );
            if(isset($insertData['password']))
            {
               $user_master_data['password'] = $insertData['password'];
            }
     $this->db->start_cache();
         $this->db->where('id',$insertData['id']);
         $this->db->update('user_master',$user_master_data);
         $this->db->stop_cache();
         $this->db->flush_cache();
     
     
         $universityData = array(
            'user_id' => $insertData['id'],
            'name' => $insertData['name'],
            'country_id' => $insertData['country_id'],
            'website' => $insertData['website'],
            'about' 		=>  $insertData['about'],
            'establishment_year' 		=>  $insertData['establishment_year'],
            'type' 			=>  $insertData['university_type'],
            'carnegie_accreditation' 	=>  $insertData['carnegie_accreditation'],
            'total_students' 		=>	$insertData['total_students'],
            'total_students_UG' 			=>	$insertData['total_students_UG'],
            'total_students_PG' 		=>	$insertData['total_students_PG'],
            'total_students_international' 			=>	$insertData['total_students_international'],
            'ranking_usa' 			=>	$insertData['ranking_usa'],
            'ranking_world' 			=>	$insertData['ranking_world'],
            'admission_success_rate' 			=>	$insertData['admission_success_rate'],
            'unique_selling_points' 	=>	$insertData['unique_selling_points'],
            'research_spending' 	=>	$insertData['research_spending'],
            'placement_percentage' 	=>	$insertData['placement_percentage'],
            'date_updated' 	=>	$insertData['date_updated'],
            'added_by' 	=>	$insertData['added_by'],
            'updated_by' 	=>	$insertData['updated_by']
         );
     
         $this->db->start_cache();
         $this->db->select('*');
         $this->db->where('user_id',$insertData['id']);
         $this->db->from('universities');
         $result = $this->db->get()->result();
         $this->db->stop_cache();
         $this->db->flush_cache();
     
         $this->db->start_cache();
         if($result)
         {
            $this->db->where('user_id',$insertData['id']);
            $this->db->update('universities',$universityData);
         }
         else
         {
            $this->db->insert('universities',$universityData);
         }
         echo $this->db->last_query();
         $this->db->stop_cache();
         $this->db->flush_cache();
     
         $this->db->from('user_permission_course');
         $this->db->where('user_id',$insertData['id']);
         $this->db->select('*');
     
         $is_user_exists=$this->db->get()->row();
         //print_r($is_user_exists); exit;
     
         // $is_user_exists = $this->db->get_where('user_permission_course', array('user_permission_course.user_id' => $insertData['id']))->row();
           if($is_user_exists){
     
            $user_permission_course_data = array(
            'view' 	=> $insertData['view'],
            'create' 	=> $insertData['create'],
            'edit' 	=> $insertData['edit'],
            'del' 		=> $insertData['del']
             );
            $this->db->where('user_id',$insertData['id']);
            $this->db->update('user_permission_course',$user_permission_course_data);
           }else{
     
           $user_permission_course_data = array(
            'user_id'	=> $insertData['id'],
            'view' 	=> $insertData['view'],
            'create' 	=> $insertData['create'],
            'edit' 	=> $insertData['edit'],
            'del' 		=> $insertData['del']
             );
            $this->db->insert('user_permission_course', $user_permission_course_data);
     
           }
        return 'success';
       }

       public function deleteUser($id){
             $status = array('status' => '5');
            $this->db->where('id',$id);
            $this->db->update('user_master',$status);
	        return 'success';

       }
       public function getAllAlumnus()
       {
           $this->db->from('alumnus');
           $this->db->join('universities', 'universities.id = alumnus.university_id');
           $this->db->join('user_master', 'user_master.id = alumnus.user_id');
           $this->db->join('type_master', 'type_master.type_id = user_master.type');
         $this->db->join('status_master', 'status_master.id = user_master.status');
           $this->db->select('alumnus.*, user_master.name AS name, user_master.email AS email, user_master.createdate, status_master.status AS status_name, universities.name AS university_name');
           $result=$this->db->get()->result();
         return $result;
       }
}
?>